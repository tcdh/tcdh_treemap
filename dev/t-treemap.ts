/**
* TCDH Tree-Map Library 
*  --  for special representation of tree-data according to the requirements of TCDH Schnitzler-Project 
*
* using svg.js
*
* @version 1.0.0
* @author Li Sheng <sheng@uni-trier.de>
*
*/

import { TtreemapDiv } from './t-treemap-div';
import { TtreemapSVG } from './t-treemap-svg';
/**
 * 
 * 
 * 
 * @class Ttreemap
 */
export class Ttreemap {

  static leafNumberAppendText: string = '';

  /**
   * 
   * @param containerId 
   * @param data 
   * @param useLogorithms 
   * @param leafCallback 
   * @param leafImageUrl 
   */
  static init (type: string, containerId: string, data: any, calculateNodeValueBy: number, leafCallback: (evt: Event, docIds: any) => void, leafImageUrl: string): TtreemapType | null {
    if (type === 'svg') {
      let obj = new TtreemapSVG();
      obj.init(containerId, data, calculateNodeValueBy, leafCallback, leafImageUrl);
      return obj;
    } else if (type === 'div') {
      let obj = new TtreemapDiv();
      obj.leafNumberAppendText = this.leafNumberAppendText;
      obj.init(containerId, data, calculateNodeValueBy, leafCallback, leafImageUrl);
      return obj;
    }
    return null;
  }

}

export interface TtreemapType {
  data: any;
  drawarea: any;
  calculateNodeValueBy: number;
  // init(containerId: string, data: any, useLogorithms: boolean, leafCallback: (evt: Event, docIds: any) => void, leafImageUrl: string): any;
  init(containerId: string, data: any, calculateNodeValueBy: number, leafCallback: (evt: Event, docIds: any) => void, leafImageUrl: string): any;
  recreateTreeMap(): any;
  showLayerUsingNum(layerNum: number, node: any): any;
  hideAll(): any;
  showAll(node: any): any;


}




/**
 * node data: 
 * {
 *     layername: "", 
 *     children: []
 *   }
 * 
 * leaf data: 
 * {
 *     layername: "",
 *     value: number
 * }
 */
