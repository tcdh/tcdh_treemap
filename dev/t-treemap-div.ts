/**
* TCDH Tree-Map Library 
*  --  for special representation of tree-data according to the requirements of TCDH Schnitzler-Project 
*
* based on jquery 3.2
*
* @version 1.0.0
* @author Li Sheng <sheng@uni-trier.de>
*
*/

import { TtreemapType } from './t-treemap';
import * as $ from 'jquery';


/**
 * 
 * 
 * @export
 * @class TtreemapDiv
 * @implements {TtreemapType}
 */
export class TtreemapDiv implements TtreemapType {

  
  data: TtreemapNode = <TtreemapNode> {};
  container: JQuery<HTMLElement> = <JQuery<HTMLElement>> {};
  containerId = '';
  drawarea: JQuery<HTMLElement> = <JQuery<HTMLElement>> {};
  // tooltip: SVG.Text;
  // tooltipBG: SVG.Rect;
  leafImageUrl = '';
  
  maxLayerNum: number = -1;
  
  // setting parameters
  calculateNodeValueBy: number = 1; // 1: linear; 2: logarithms; 3: equal splitted
  nodeInnerLayerMargin = 4;
  innerWHMin = 0;
  
  
  // helping variables
  cWidth: number | undefined;
  cHeight: number | undefined;
  // leafPattern: SVG.Pattern;
  TitleRowMax: number = 0;

  // text showed after number of leaves
  leafNumberAppendText: string = '';

  leafCallback: (evt: any, docIds: any) => void = () => {};

  /**
   * 
   * 
   * @param {string} containerId 
   * @param {TtreemapNode} data 
   * @param {number} calculateNodeValueBy 
   * @param {(evt: Event, docIds: any) => void} leafCallback 
   * @param {string} leafImageUrl 
   * @memberof TtreemapDiv
   */
  init (containerId: string, data: TtreemapNode, calculateNodeValueBy: number, leafCallback: (evt: Event, docIds: any) => void, leafImageUrl: string) {
    this.calculateNodeValueBy = calculateNodeValueBy;
    this.container = $('#' + containerId);
    this.containerId = containerId;
    this.data = data;
    this.leafImageUrl = leafImageUrl;
    this.leafCallback = leafCallback;
    this.cWidth = this.container.width();
    this.cHeight = this.container.height();
    this.maxLayerNum = -1;
    this.recreateTreeMap();
  }

  /**
   * 
   * 
   * @memberof TtreemapDiv
   */
  recreateTreeMap () {
    // delete all layers
    if (this.drawarea && this.drawarea.remove) {
      this.drawarea.remove();
    }
    this.drawarea = $(`<div class="ttm_div_drawarea" id="${this.containerId + 'drawarea'}"></div>`);
    this.container.append(this.drawarea);
    const _this = this;
    this.calculateNodeValue(this.data, null, 0);
    const layers = this.createTreeMap(this.data, this.drawarea, {
      width: this.cWidth,
      height: this.cHeight,
      parentNodeChildrenLength: 0
    }, 0, null, null);
    // check size
    this.checkAndAdjustSize(this.data);
  }

  /**
   * 
   * 
   * @param {(TtreemapNode | TtreemapLeaf)} node 
   * @param {(TtreemapNode | null)} parentNode 
   * @param {number} pos 
   * @returns 
   * @memberof TtreemapDiv
   */
  calculateNodeValue (node: TtreemapNode | TtreemapLeaf, parentNode: TtreemapNode | null, pos: number) {
    let oriValue = 0;
    let normValue = 0;
    let equalSplitValue = 1;
    let normValueFromBottom = 0;
    node.tooltip = this.replaceBRtags(node.tooltip);
    if (parentNode !== null) {
      equalSplitValue = 1.0 / parentNode.children.length;
    }
    node.pos = pos;
    if ((<TtreemapNode>node).children) {
      let tempOriValue = 0;
      let tempNormValue = 0;
      let tempNormValueFromBottom = 0;
      for (let i = 0; i < (<TtreemapNode>node).children.length; i++) {
        let vs = this.calculateNodeValue((<TtreemapNode>node).children[i], <TtreemapNode>node, i);
        tempOriValue += vs[0];
        tempNormValueFromBottom += vs[2];
      }
      oriValue += tempOriValue;
      if (oriValue < 2) { // too small value
        normValue = this.calculateLogarithms(2);
      } else {
        normValue = this.calculateLogarithms(oriValue);
      }
      // console.log(node.nodename + " " + oriValue + "  " + normValue);
      normValueFromBottom += tempNormValueFromBottom;
    } else {
      oriValue = (<TtreemapLeaf>node).value;
      if ((<TtreemapLeaf>node).value < 2) { // too small value
        normValue = this.calculateLogarithms(2);
      } else {
        normValue = this.calculateLogarithms((<TtreemapLeaf>node).value);
      }
      normValueFromBottom = normValue;
    }
    node.treemapValues = [oriValue, normValue, normValueFromBottom, equalSplitValue];
    return node.treemapValues;
  }

  /**
   * 
   * 
   * @param {(TtreemapNode | TtreemapLeaf)} node 
   * @returns 
   * @memberof TtreemapDiv
   */
  calculateNodeValueOnlyOneLayer (node: TtreemapNode | TtreemapLeaf) {
    let oriValue = node.treemapValues ? node.treemapValues[0] : 0;
    let normValue = node.treemapValues ? node.treemapValues[1] : 0;
    let normValueFromNextLayer = 0;
    if ((<TtreemapNode>node).children) {
      let tempNormValueFromNextLayer = 0;
      for (let i = 0; i < (<TtreemapNode>node).children.length; i++) {
        let vs = (<TtreemapNode>node).children[i].treemapValues;
        tempNormValueFromNextLayer += vs ? vs[1] : 0;
      }
      normValueFromNextLayer += tempNormValueFromNextLayer;
    } else {
      normValueFromNextLayer = node.treemapValues ? node.treemapValues[2] : 0;
    }
    return [oriValue, normValueFromNextLayer];
  }
  
  /**
   * 
   * 
   * @param {(TtreemapNode | TtreemapLeaf)} node 
   * @memberof TtreemapDiv
   */
  checkAndAdjustSize(node: TtreemapNode | TtreemapLeaf) {
    const nodeWidth = node.layers.innerLayer.width();
    const nodeHeight = node.layers.innerLayer.height();
    if ((<TtreemapNode>node).children) {
      if (<number>node.layers.innerLayer.height() + <number>node.layers.titleDiv.height() > <number>node.layers.outerLayer.height()) {
        let newHeight = <number>node.layers.outerLayer.height() - (<number>node.layers.titleDiv.height());
        node.layers.innerLayer.height(newHeight);
        if ((<TtreemapNode>node).children) {
          for (let i = 0; i < (<TtreemapNode>node).children.length; i++) {
            const child = (<TtreemapNode>node).children[i];
            let childNewHeight = newHeight;
            if (node.layerNum % 2 === 1) {
              const parentNodeChildrenLength = (<TtreemapNode>node).children.length;
              childNewHeight = (newHeight - this.nodeInnerLayerMargin * (parentNodeChildrenLength - 1)) * (<any>child.treemapValues)[3];
            } else {
            }
            child.layers.outerLayer.height(childNewHeight);
          }
        }
      }
    } 
    if ((<TtreemapNode>node).children) {
      for (let i = 0; i < (<TtreemapNode>node).children.length; i++) {
        const child = (<TtreemapNode>node).children[i];
        this.checkAndAdjustSize(child);
      }
    }
  }

  /**
   * 
   * 
   * @param {(TtreemapNode | TtreemapLeaf)} node 
   * @param {JQuery<HTMLElement>} parentDiv 
   * @param {*} parentInfo 
   * @param {number} layerNum 
   * @param {(TtreemapNode | TtreemapLeaf | null)} parentNode 
   * @param {(CreateLayerReturnType|null)} lsls 
   * @returns {CreateLayerReturnType} 
   * @memberof TtreemapDiv
   */
  createTreeMap (node: TtreemapNode | TtreemapLeaf, parentDiv: JQuery<HTMLElement>, parentInfo: any, layerNum: number, parentNode: TtreemapNode | TtreemapLeaf | null, lsls: CreateLayerReturnType|null): CreateLayerReturnType {
    let parentWidth = parentInfo.width;
    let parentHeight = parentInfo.height;
    let parentNodeChildrenLength = parentInfo.parentNodeChildrenLength;
    let isVertical = false;
    if (layerNum % 2 === 1) {
      isVertical = true;
    } else {
      isVertical = false;
    }
    let useValueNum = 0;
    if (this.calculateNodeValueBy === 2) {
      useValueNum = 1;
    }
    if (this.calculateNodeValueBy === 3) {
      useValueNum = 3;
    }
    let parentValue = parentNode !== null ? this.calculateNodeValueOnlyOneLayer(parentNode)[useValueNum] : this.calculateNodeValueOnlyOneLayer(node)[useValueNum];

    // calculate node width and height
    let nodeValue = node.treemapValues ? node.treemapValues[useValueNum] : 0;
    let nodeWidth = 0, nodeHeight = 0, marginTop = 0, marginLeft = 0;
    if (isVertical) {
      nodeWidth = 1.0 * nodeValue / parentValue * (parentWidth - this.nodeInnerLayerMargin * (parentNodeChildrenLength + 1)) ;
      if (this.calculateNodeValueBy === 3) {
        nodeWidth = (parentWidth - this.nodeInnerLayerMargin * (parentNodeChildrenLength + 1)) * nodeValue;
        marginLeft = this.nodeInnerLayerMargin * (node.pos + 1) + nodeWidth * node.pos;
      }
      marginTop = 0;

      nodeHeight = parentHeight;

    } else {
      nodeWidth = parentWidth - this.nodeInnerLayerMargin * 2;
      nodeHeight = 1.0 * nodeValue / parentValue * (parentHeight - this.nodeInnerLayerMargin * (parentNodeChildrenLength + 1));
      if (this.calculateNodeValueBy === 3) {
        nodeHeight = (parentHeight - this.nodeInnerLayerMargin * (parentNodeChildrenLength - 1)) * nodeValue;
      }
      if (node.pos === 0) {
        marginTop = 0;
      } else {
        marginTop = this.nodeInnerLayerMargin;
      }

    }
    if (!parentNode) {
      nodeWidth = parentWidth ;
      nodeHeight = parentHeight;
    }

    // create top layer
    let inOutLayers = this.createLayer(node, parentNode, parentDiv, layerNum, nodeWidth, nodeHeight, marginTop, marginLeft);
    let innerLayer = inOutLayers.innerLayer;
    let outerLayer = inOutLayers.outerLayer;

    // check if child exists
    if ((<TtreemapNode>node).children) {
      let lastSiblingLayers = null;

      for (let i = 0; i < (<TtreemapNode>node).children.length; i++) {
        let child = (<TtreemapNode>node).children[i];
        lastSiblingLayers = this.createTreeMap(child, innerLayer, {
          width: innerLayer.width(),
          height: innerLayer.height(),
          parentNodeChildrenLength: (<TtreemapNode>node).children.length
        }, layerNum + 1, node, lastSiblingLayers);
        innerLayer.append(lastSiblingLayers.outerLayer);
      }
    } else {
      let docIds = (<TtreemapLeaf>node).docIdList;
      inOutLayers.innerLayer.click((evt) => {
        evt.stopPropagation();
        this.leafCallback(evt, docIds);
      });

    }
    return inOutLayers;
  }

  /**
   * 
   * 
   * @param {(TtreemapNode | TtreemapLeaf)} node 
   * @param {(TtreemapNode | TtreemapLeaf | null)} parentNode 
   * @param {JQuery<HTMLElement>} parentDiv 
   * @param {number} layerNum 
   * @param {number} layerWidth 
   * @param {number} layerHeight 
   * @param {number} marginTop 
   * @param {number} marginLeft 
   * @returns {CreateLayerReturnType} 
   * @memberof TtreemapDiv
   */
  createLayer (node: TtreemapNode | TtreemapLeaf, parentNode: TtreemapNode | TtreemapLeaf | null, parentDiv: JQuery<HTMLElement>, layerNum: number, layerWidth: number, layerHeight: number, marginTop: number, marginLeft: number): CreateLayerReturnType {
    let outerLayer = $('<div></div>')
      // .width(layerWidth).height(layerHeight)
      // .css('flex-grow', '' + (node.treemapValues ? node.treemapValues[2] : 1))
      .addClass('ttm_div_layer')
      .addClass('ttm_div_layer_' + layerNum)
      .css('width', layerWidth)
      .css('height', layerHeight)
      .css('margin-top', marginTop)
      // .css('margin-right', 0)
      .css('float', 'left')
      .attr('title', node.tooltip)
      ;
    if (parentNode) {
      outerLayer.css('margin-left', this.nodeInnerLayerMargin);
    }
    parentDiv.append(outerLayer);
    // title div
    let oriText = node.nodename;
    if (node.showValue) {
      oriText += '<span class="test"> (' + (node.treemapValues ? node.treemapValues[0] : 0) + '<span>' + this.leafNumberAppendText + '</span>' + ')</span>';
    } 
    let oriTextForTitle = this.replaceBRtags(oriText);
    let titleDiv = $('<div></div>')
      // .width(layerWidth)
      .addClass('ttm_div_title').addClass('ttm_div_title_' + layerNum);
    let titleSpan = $('<span class="ttm_div_title_span"></span>').html(oriTextForTitle);
    titleDiv.append(titleSpan);
    titleDiv.attr('title', node.tooltip);
    if ((<TtreemapNode>node).children) {
      outerLayer.append(titleDiv);
    } else {
      titleDiv.height(0);
    }

    // try to make text shorter
    // let widthToShowText = outerLayer.bbox().width;
    // if ((<TtreemapNode>node).children) {
    //   widthToShowText = outerLayer.bbox().width - innerHeightTop - expandBtnPathGap * 2;
    // }
    // if (titleText.bbox().width > widthToShowText) {
    //   let percentOfToCutText = 1.0 * (titleText.bbox().width - widthToShowText) / titleText.bbox().width;
    //   let numToCut = Math.ceil(percentOfToCutText * node.nodename.length) + 4; // 3 points '...'
    //   let shortText = '...';
    //   if (numToCut < node.nodename.length) {
    //     shortText = node.nodename.substring(0, node.nodename.length - numToCut) + '...';
    //   }
    //   titleText.plain(shortText + ' (' + (node.treemapValues ? node.treemapValues[0] : 0) + ')');
    // }

    // inner layer
    let innerLayer = $('<div></div>')
      .addClass('ttm_div_layer_inner').addClass('ttm_div_layer_inner_' + layerNum);
    if (layerNum % 2 === 0) {
      innerLayer.addClass('ttm_div_layer_inner_horizontal');
    } else {
      innerLayer.addClass('ttm_div_layer_inner_vertical');
    }
    let innerLayerHeight = layerHeight - <number>titleDiv.height();
    if ((<TtreemapNode>node).children) {
      innerLayerHeight -= this.nodeInnerLayerMargin;
    }
    innerLayer
    .css('width', layerWidth)
    .css('height', innerLayerHeight
    );
    outerLayer.append(innerLayer);

    innerLayerHeight = <number>innerLayer.height();
    let innerLayerWidth = <number>innerLayer.width();
    // add inner title text
    let innerTitleText = $(`<span></span>`).html(oriText)
    .addClass('innerTitleText');
    // check if should using vertical text
    if ((<TtreemapNode>node).children && innerLayerHeight / innerLayerWidth * 1.0 > 1.2) {
      innerTitleText.addClass('vertcalTitleText');
    }
    innerLayer.append(innerTitleText);
    // caculate title size
    let titleTextHeight = <number>innerTitleText.height();
    let titleTextWidth = <number>innerTitleText.width();
    let limit = 0.8; // max 80% of inner layer width or height
    let fontSize = 5, maxFontSize = 50;
    if ((<TtreemapNode>node).children) {
      if (innerLayerHeight / innerLayerWidth * 1.0 > 1) {
        innerTitleText.addClass('vertcalTitleText');
      }
      while (titleTextHeight < innerLayerHeight * limit && titleTextWidth < innerLayerWidth * limit && fontSize < maxFontSize) {
        fontSize++;
        innerTitleText.css('font-size', fontSize + 'px');
        titleTextHeight = <number>innerTitleText.height();
        titleTextWidth = <number>innerTitleText.width();
      }
    } else {
      fontSize = 5, maxFontSize = 20;
      innerTitleText.addClass('innerTitleText_last');
      innerTitleText.css('font-size', fontSize + 'px');
      titleTextHeight = <number>innerTitleText.height();
      titleTextWidth = <number>innerTitleText.width();
      while (titleTextHeight < innerLayerHeight * limit && titleTextWidth < innerLayerWidth * limit && fontSize < maxFontSize) {
        fontSize++;
        if (fontSize < 12) {
          limit = 0.9;
        } else {
          limit = 0.8;
        }
        innerTitleText.css('font-size', fontSize + 'px');
        titleTextHeight = <number>innerTitleText.height();
        titleTextWidth = <number>innerTitleText.width();
      }
      // add background image in leaf
      const bgUrl = (<TtreemapLeaf>node).backgroundImageUrl;
      if (bgUrl) {
        innerLayer.css('background-image', 'url(' + <string>bgUrl + ')');
      }
    }
    titleTextHeight = <number>innerTitleText.height();
    titleTextWidth = <number>innerTitleText.width();
    marginLeft = (innerLayerWidth - titleTextWidth) / 2;
    marginTop = (innerLayerHeight - titleTextHeight) / 2;
    if (marginLeft < 0) {
      marginLeft = 0;
    }
    if (marginTop < 0) {
      marginTop = 0;
    }
    innerTitleText
    .css('margin-left', marginLeft)
    .css('margin-top', marginTop)
    ;
    // adjust offset of inner title
    const titleTextOffsetTop = (<JQuery.Coordinates>innerTitleText.offset()).top;
    const innerLayerOffsetTop = (<JQuery.Coordinates>innerLayer.offset()).top;

    if (titleTextOffsetTop !== innerLayerOffsetTop) {
      innerTitleText.offset({
        top: innerLayerOffsetTop + marginTop,
        left: (<JQuery.Coordinates>innerTitleText.offset()).left
      });
    }

    // add image to inner layer if the node is a leaf
    if (!(<TtreemapNode>node).children) {
      innerLayer.addClass('ttm_div_layer_inner_last');
      if (this.leafImageUrl !== '') {
        // use user defined image
        innerLayer.attr('background-image', 'url("' + this.leafImageUrl + '")');
      }
    }

    let expandBtn = null;
    // show-hide button
    if ((<TtreemapNode>node).children) {
      expandBtn = $('<div></div>')
      .addClass('tmm_expand_btn_expand')
      .addClass('tmm_expand_btn_expand_minus_' + layerNum);
      titleDiv.append(expandBtn);
      (<TtreemapNode>node).expanded = true;
      (<TtreemapNode>node).expandButton = expandBtn;

      // clicking on node opens its under layer
      outerLayer.click((evt) => {
        evt.stopPropagation();
        this.expandLayer(node);
      });
      innerLayer.click((evt) => {
        evt.stopPropagation();
        this.expandLayer(node);
      });
      titleDiv.click((evt) => {
        evt.stopPropagation();
        this.expandLayer(node);
      });
    }
    let ret: CreateLayerReturnType = {
      innerLayer: innerLayer,
      titleDiv: titleDiv,
      titleSpan: titleSpan,
      outerLayer: outerLayer,
      innerTitleText: innerTitleText
    };
    node.layers = ret;
    if (this.maxLayerNum < layerNum) {
      this.maxLayerNum = layerNum;
    }
    node.layerNum = layerNum;

    return ret;

  }

  /**
   * 
   * 
   * @param {(TtreemapNode | TtreemapLeaf)} node 
   * @memberof TtreemapDiv
   */
  expandLayer (node: TtreemapNode | TtreemapLeaf) {
    if ((<TtreemapNode>node).expanded) {
      this.hideLayer(node);
    } else {
      this.showLayer(node);
    }

  }

  /**
   * 
   * 
   * @param {(TtreemapNode | TtreemapLeaf)} node 
   * @memberof TtreemapDiv
   */
  hideLayer (node: TtreemapNode | TtreemapLeaf) {
    // console.log(node.nodename + ': ' + node.expanded);
    if ((<TtreemapNode>node).children) {
      for (let i = 0; i < (<TtreemapNode>node).children.length; i++) {
        let child = (<TtreemapNode>node).children[i];
        child.layers ? child.layers.outerLayer.hide() : false;
        if ((<TtreemapNode>child).children) {
          const tcb = (<TtreemapNode>child).expandButton;
        }
        this.hideLayer(child);
      }
    }
    (<TtreemapNode>node).expanded = false;
    if ((<TtreemapNode>node).children) {
      const tcb = (<TtreemapNode>node).expandButton;
      tcb ? tcb.removeClass('tmm_expand_btn_expand_minus_' + node.layerNum) : true;
      tcb ? tcb.addClass('tmm_expand_btn_expand_plus_' + node.layerNum) : true;
      tcb ? tcb.show() : true;
    }
    // hide vertical text
    node.layers.innerTitleText.show();
    // hide title
    node.layers.titleSpan.hide();
    const nodeExpandButton = (<TtreemapNode>node).expandButton;
    nodeExpandButton ? nodeExpandButton.hide() : '';
  }

  /**
   * always show the first layer under this node
   * 
   * @param {(TtreemapNode | TtreemapLeaf)} node 
   * @memberof TtreemapDiv
   */
  showLayer (node: TtreemapNode | TtreemapLeaf) {
    // console.log(node.nodename + ': ' + node.expanded);
    if ((<TtreemapNode>node).children) {
      for (let i = 0; i < (<TtreemapNode>node).children.length; i++) {
        let child = (<TtreemapNode>node).children[i];
        child.layers ? child.layers.outerLayer.show() : false;
        if ((<TtreemapNode>child).children) {
          const tcb = (<TtreemapNode>child).expandButton;
          tcb ? tcb.removeClass('tmm_expand_btn_expand_minus_' + child.layerNum) : true;
          tcb ? tcb.addClass('tmm_expand_btn_expand_plus_' + child.layerNum) : true;
          tcb ? tcb.hide() : true;
        }
        
      }
    }
    (<TtreemapNode>node).expanded = true;
    if ((<TtreemapNode>node).children) {
      const tcb = (<TtreemapNode>node).expandButton;
      tcb ? tcb.removeClass('tmm_expand_btn_expand_plus_' + node.layerNum) : true;
      tcb ? tcb.addClass('tmm_expand_btn_expand_minus_' + node.layerNum) : true;
      tcb ? tcb.show() : true;
      // hide vertical text
      node.layers.innerTitleText.hide();
      // show title
      node.layers.titleSpan.show();
      const nodeExpandButton = (<TtreemapNode>node).expandButton;
      nodeExpandButton ? nodeExpandButton.show() : '';
    }
    
  }

  /**
   * 
   * 
   * @returns 
   * @memberof TtreemapDiv
   */
  getMaxLayerNum () {
    return this.maxLayerNum;
  }

  /**
   * 
   * 
   * @param {number} layerNum 
   * @param {(TtreemapNode | TtreemapLeaf)} node 
   * @memberof TtreemapDiv
   */
  showLayerUsingNum (layerNum: number, node: TtreemapNode | TtreemapLeaf) {
    
    if (typeof node === 'undefined') {
      this.hideAll();
    }
    node = typeof node !== 'undefined' ?  node : this.data;
    if (node.layerNum <= layerNum) {
      this.showLayer(node);
    }
    if ((<TtreemapNode>node).children) {
      for (let i = 0; i < (<TtreemapNode>node).children.length; i++) {
        let child = (<TtreemapNode>node).children[i];
        this.showLayerUsingNum(layerNum, child);
        
      }
    }
  }

  /**
   * 
   * 
   * @memberof TtreemapDiv
   */
  hideAll () {
    this.hideLayer(this.data);
  }

  /**
   * 
   * 
   * @param {(TtreemapNode | TtreemapLeaf)} node 
   * @memberof TtreemapDiv
   */
  showAll (node: TtreemapNode | TtreemapLeaf) {
    node = typeof node !== 'undefined' ?  node : this.data;
    this.showLayer(node);
    if ((<TtreemapNode>node).children) {
      for (let i = 0; i < (<TtreemapNode>node).children.length; i++) {
        let child = (<TtreemapNode>node).children[i];
        this.showAll(child);
        
      }
    }
  }

  /**
   * 
   * 
   * @param {number} u 
   * @memberof TtreemapDiv
   */
  setBlockCalcMethod (u: number) {
    this.calculateNodeValueBy = u;
  }

  /**
   * 
   * 
   * @param {number} n 
   * @returns 
   * @memberof TtreemapDiv
   */
  calculateLogarithms (n: number) {
    // return Math.log(n);
    // return Math.log(n) / Math.log(10);
    // return Math.log(n) / Math.LN10;
    if (n < 10) {
      return 4;
    } else if (n < 100) {
      return 6;
    } else if (n < 1000) {
      return 8;
    } else {
      return 10;
    }

  }

  /**
   * 
   * 
   * @private
   * @param {(JQuery<HTMLElement> | undefined)} outerLayer 
   * @returns 
   * @memberof TtreemapDiv
   */
  private getLayerWidth(outerLayer: JQuery<HTMLElement> | undefined) {
    const res = (outerLayer !== undefined ? outerLayer.width() : 0);
    if (res !== undefined) {
      return res;
    } else {
      return 0;
    }
  }

  replaceBRtags(str: string) {
    if (str) {
      str = str.replace(/<br>/g, '');
      str = str.replace(/<br\/>/g, '');
    }
    return str;
  }
}

export interface CreateLayerReturnType {
  innerLayer: JQuery<HTMLElement>;
  titleDiv: JQuery<HTMLElement>;
  titleSpan: JQuery<HTMLElement>;
  outerLayer: JQuery<HTMLElement>;
  innerTitleText: JQuery<HTMLElement>;
}
export interface TtreemapNode {
  nodename: string;
  pos: number;
  children: TtreemapNode[] | TtreemapLeaf[];
  expandButton?: JQuery<HTMLElement>;
  expanded?: boolean;
  treemapValues?: number[];
  layers: CreateLayerReturnType;
  layerNum: number;
  tooltip: string;
  showValue: boolean;
}

export interface TtreemapLeaf {
  nodename: string;
  pos: number;
  value: number;
  docIdList?: number[];
  treemapValues?: number[];
  layers: CreateLayerReturnType;
  firstId: number;
  layerNum: number;
  tooltip: string;
  backgroundImageUrl?: string;
  showValue: boolean;
}



/**
 * node data: 
 * {
 *     layername: "", 
 *     children: []
 *   }
 * 
 * leaf data: 
 * {
 *     layername: "",
 *     value: number
 * }
 */


