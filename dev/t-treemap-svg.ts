/**
* TCDH Tree-Map Library 
*  --  for special representation of tree-data according to the requirements of TCDH Schnitzler-Project 
*
* using svg.js
*
* @version 1.0.0
* @author Li Sheng <sheng@uni-trier.de>
*
*/

import { TtreemapType } from './t-treemap';
import * as SVG from 'svg.js';
/**
 * 
 * 
 * 
 * @class TtreemapSVG
 */
export class TtreemapSVG implements TtreemapType {

  
  data: TtreemapNode = <TtreemapNode> {};
  container: HTMLElement | null = <HTMLElement> {};
  containerId = '';
  drawarea: SVG.Doc = <SVG.Doc> {};
  tooltip: SVG.Text = <SVG.Text> {};
  tooltipBG: SVG.Rect = <SVG.Rect> {};
  leafImageUrl = '';
  
  maxLayerNum = -1;
  
  // setting parameters
  calculateNodeValueBy: number = 1; // 1: linear; 2: logarithms; 3: equal splitted
  innerHightPercentTop = 0.03;
  innerHightPercentBottom = 0.01;
  innerWidthPercentBoth = 0.01;
  innerWHMin = 0;
  titlePointGap = 3;
  TitleRowMin = 20;
  TitleRowMax: number = 30;
  
  
  // helping variables
  cWidth: number = 0;
  cHeight: number = 0;
  leafPattern: SVG.Pattern = <SVG.Pattern> {};

  leafCallback: (evt: Event, docIds: any) => void = () => {};

  /**
   * 
   * @param containerId 
   * @param data 
   * @param useLogorithms 
   * @param leafCallback 
   * @param leafImageUrl 
   */
  init (containerId: string, data: any, calculateNodeValueBy: number, leafCallback: (evt: Event, docIds: any) => void, leafImageUrl: string) {
    this.calculateNodeValueBy = calculateNodeValueBy;
    this.container = document.getElementById(containerId);
    this.containerId = containerId;
    this.data = data;
    this.leafImageUrl = leafImageUrl;
    this.leafCallback = leafCallback;
    this.cWidth = (<HTMLElement>this.container).clientWidth;
    this.cHeight = (<HTMLElement>this.container).clientHeight;
    this.maxLayerNum = -1;
    this.recreateTreeMap();
  }

  recreateTreeMap () {
    // delete all layers
    if (this.drawarea) {
      this.drawarea.remove();
    }
    this.drawarea = SVG(this.containerId).size(this.cWidth, this.cHeight).attr('id', this.containerId + '_drawarea');
    // let defs = SVG.Defs();
    // console.log(defs);
    // console.log(this.drawarea);
    const _this = this;
    if (this.leafImageUrl) {
      this.leafPattern = this.drawarea.pattern(1, 1, function(add) {
        add.image(_this.leafImageUrl);
      }).attr('id', 'ttm_svg_leaf_image')
      .attr('patternUnits', 'objectBoundingBox');
    }
    this.calculateNodeValue(this.data, null);
    this.createTreeMap(this.data, {
      width: this.cWidth,
      height: this.cHeight,
      x: 0,
      y: 0
    }, 0, null, null);
  }

  /**
   * 
   * 
   * @param {any} node
   * @returns number
   */
  calculateNodeValue (node: TtreemapNode | TtreemapLeaf, parentNode: TtreemapNode | null) {
    // let oriValue = typeof values !== 'undefined' ?  values[0] : 0;
    // let normValue = typeof values !== 'undefined' ?  values[1] : 0;
    let oriValue = 0;
    let normValue = 0;
    let equalSplitValue = 1;
    let normValueFromBottom = 0;
    if (parentNode !== null) {
      equalSplitValue = 1.0 / parentNode.children.length;
    }
    if ((<TtreemapNode>node).children) {
      let tempOriValue = 0;
      let tempNormValue = 0;
      let tempNormValueFromBottom = 0;
      for (let i = 0; i < (<TtreemapNode>node).children.length; i++) {
        let vs = this.calculateNodeValue((<TtreemapNode>node).children[i], <TtreemapNode>node);
        tempOriValue += vs[0];
        tempNormValueFromBottom += vs[2];
      }
      oriValue += tempOriValue;
      if (oriValue < 2) { // too small value
        normValue = this.calculateLogarithms(2);
      } else {
        normValue = this.calculateLogarithms(oriValue);
      }
      // console.log(node.nodename + " " + oriValue + "  " + normValue);
      normValueFromBottom += tempNormValueFromBottom;
    } else {
      oriValue = (<TtreemapLeaf>node).value;
      if ((<TtreemapLeaf>node).value < 2) { // too small value
        normValue = this.calculateLogarithms(2);
      } else {
        normValue = this.calculateLogarithms((<TtreemapLeaf>node).value);
      }
      normValueFromBottom = normValue;
    }
    node.treemapValues = [oriValue, normValue, normValueFromBottom, equalSplitValue];
    return node.treemapValues;
  }

  /**
   * 
   * 
   * @param {any} node
   * @returns number
   */
  calculateNodeValueOnlyOneLayer (node: TtreemapNode | TtreemapLeaf) {
    let oriValue = node.treemapValues ? node.treemapValues[0] : 0;
    let normValue = node.treemapValues ? node.treemapValues[1] : 0;
    let normValueFromNextLayer = 0;
    if ((<TtreemapNode>node).children) {
      let tempNormValueFromNextLayer = 0;
      for (let i = 0; i < (<TtreemapNode>node).children.length; i++) {
        let vs = (<TtreemapNode>node).children[i].treemapValues;
        tempNormValueFromNextLayer += vs ? vs[1] : 0;
      }
      normValueFromNextLayer += tempNormValueFromNextLayer;
    } else {
      normValueFromNextLayer = node.treemapValues ? node.treemapValues[2] : 0;
    }
    return [oriValue, normValueFromNextLayer];
  }

  /**
   * 
   * 
   * @param {any} node
   * @param {any} parentInfo
   * @param {any} layerNum
   * @param {any} parentNode
   * @param {any} lsls -- lastSiblingLayers
   * @returns array
   */
  createTreeMap (node: TtreemapNode | TtreemapLeaf, parentInfo: any, layerNum: number, parentNode: TtreemapNode | TtreemapLeaf | null, lsls: CreateLayerReturnType|null): CreateLayerReturnType {
    let parentWidth = parentInfo.width;
    let parentHeight = parentInfo.height;
    let parentX = parentInfo.x;
    let parentY = parentInfo.y;
    let isVertical = false;
    if (layerNum % 2 === 1) {
      isVertical = true;
    } else {
      isVertical = false;
    }
    let useValueNum = 0;
    if (this.calculateNodeValueBy === 2) {
      useValueNum = 1;
    }
    if (this.calculateNodeValueBy === 3) {
      useValueNum = 3;
    }
    let parentValue = parentNode !== null ? this.calculateNodeValueOnlyOneLayer(parentNode)[useValueNum] : this.calculateNodeValueOnlyOneLayer(node)[useValueNum];
    let parentValueList = parentNode !== null ? this.calculateNodeValueOnlyOneLayer(parentNode) : this.calculateNodeValueOnlyOneLayer(node);
    let x = 0, y = 0;
    if (isVertical) {
      x = lsls !== null ? (lsls.layerGroup.x() + lsls.outerLayer.width()) : parentX;
      y = parentY;

    } else {
      x = parentX;
      y = lsls !== null ? (lsls.layerGroup.y() + lsls.outerLayer.height()) : parentY;

    }

    // calculate node width and height
    let nodeValue = node.treemapValues ? node.treemapValues[useValueNum] : 0;
    let nodeWidth = 0, nodeHeight = 0;
    if (isVertical) {
      nodeWidth = 1.0 * nodeValue / parentValue * parentWidth ;
      if (this.calculateNodeValueBy === 3) {
        nodeWidth = parentWidth * nodeValue;
      }
      nodeHeight = parentHeight;

    } else {
      nodeWidth = parentWidth;
      nodeHeight = 1.0 * nodeValue / parentValue * parentHeight;
      if (this.calculateNodeValueBy === 3) {
        nodeHeight = parentHeight * nodeValue;
      }

    }
    if (!parentNode) {
      nodeHeight = parentHeight;
    }

    // if (node.nodename === 'Drucke' || node.nodename ==='Eigenadaptionen') {
    //   console.log(parentValueList);
    //   console.log(node.nodename + '  ' + nodeValue + '  ' + parentValue);
    //   console.log(node.nodename + '  ' + isVertical);
    //   console.log('parentWidth + parentHeight:  ' + parentWidth + '  ' + parentHeight);
    //   console.log('parentX+ parentY:  '+parentX + '  ' + parentY);
    //   console.log('x + y:  '+ x + '  ' + y);
    //   console.log(nodeHeight);
    //   console.log('------------------------------');
    // }
    // create top layer
    let inOutLayers = this.createLayer(node, layerNum, nodeWidth, nodeHeight, x, y);
    let innerLayer = inOutLayers.innerLayer;
    let outerLayer = inOutLayers.outerLayer;
    let layerGroup = inOutLayers.layerGroup;
    let innerHeightTop = inOutLayers.innerHeightTop;

    // console.log(node.nodename + ' nodeWidth + nodeHeight:  ' + nodeWidth + '    '+ nodeHeight);
    // console.log(node.nodename + ' innerLayer.width()+ innerLayer.height():  ' + innerLayer.width() + '    '+ innerLayer.height());
    // console.log(node.nodename + ' innerLayer.x()+ innerLayer.y():  ' + innerLayer.x() + '    '+ innerLayer.y());
    // console.log(node.nodename + ' layerGroup.x()+ layerGroup.y():  ' + layerGroup.x() + '    '+ layerGroup.y());
    // console.log('');

    // check if child exists
    if ((<TtreemapNode>node).children) {
      let lastSiblingLayers = null;
      // let lastSiblingInnerLayer = null;
      // let lastSiblingOuterLayer = null;
      // let lastSiblingLayerGroup = null;

      for (let i = 0; i < (<TtreemapNode>node).children.length; i++) {
        let child = (<TtreemapNode>node).children[i];
        lastSiblingLayers = this.createTreeMap(child, {
          width: innerLayer.width(),
          height: innerLayer.height(),
          x: layerGroup.x() + (outerLayer.width() - innerLayer.width()) / 2,
          y: layerGroup.y() + innerHeightTop
        }, layerNum + 1, node, lastSiblingLayers);
        // lastSiblingInnerLayer = lastSiblingLayers[0];
        // lastSiblingOuterLayer = lastSiblingLayers[1];
        // lastSiblingLayerGroup = lastSiblingLayers[2];
        // console.log(child.nodename + ' lastSiblingLayer:  ' + lastSiblingOuterLayer.width() + '  ' + lastSiblingOuterLayer.height());
        // console.log(child.nodename + ' lastSiblingLayer:  ' + lastSiblingLayerGroup.x() + '  ' + lastSiblingLayerGroup.y());
        // console.log('');
      }
    } else {
      // add class innerlayer_last for leaf
      // inOutLayers[0].addClass('ttm_svg_layer_inner_last');
      let docIds = (<TtreemapLeaf>node).docIdList;
      inOutLayers.innerLayer.click((evt: Event) => {
        this.leafCallback(evt, docIds);
      });
      if (this.leafImageUrl) {
        inOutLayers.innerLayer.attr('fill', 'url(#ttm_svg_leaf_image)');
      }

    }
    return inOutLayers;
  }

  /**
   * 
   * 
   * @param {any} node
   * @param {any} layerNum
   * @param {any} layerWidth
   * @param {any} layerHeight
   * @param {any} x
   * @param {any} y
   * @returns array
   */
  createLayer (node: TtreemapNode | TtreemapLeaf, layerNum: number, layerWidth: number, layerHeight: number, x: number, y: number): CreateLayerReturnType {
    let layerGroup = this.drawarea.group().addClass('ttm_svg_layerGroup_' + layerNum).dx(x).dy(y);
    let outerLayer = this.drawarea.rect(layerWidth, layerHeight).addClass('ttm_svg_layer').addClass('ttm_svg_layer_' + layerNum);
    layerGroup.add(outerLayer);

    // inner rect
    let innerHeightTop = this.innerHightPercentTop * layerHeight;
    let innerHeightBottom = this.innerHightPercentBottom * layerHeight;
    let innerWidthBoth = this.innerWidthPercentBoth * layerWidth;
    if (innerHeightTop < this.TitleRowMin) {
      innerHeightTop = this.TitleRowMin;
    }
    if (innerHeightTop > this.TitleRowMax) {
      innerHeightTop = this.TitleRowMax;
    }
    if (innerHeightBottom < 5) {
      innerHeightBottom = 5;
    }
    if (innerHeightBottom > 15) {
      innerHeightBottom = 15;
    }
    if (innerWidthBoth < 5) {
      innerWidthBoth = 5;
    }
    if (innerWidthBoth > 15) {
      innerWidthBoth = 15;
    }
    // innerHeightTop = 0, innerHeightBottom = 0, innerWidthBoth = 0;

    let innerHeight = layerHeight - innerHeightTop - innerHeightBottom;
    let innerWidth = layerWidth - innerWidthBoth;

    if (innerHeight < this.innerWHMin) {
      innerHeight = this.innerWHMin;
    }
    
    if (innerWidth < this.innerWHMin) {
      innerWidth = this.innerWHMin;
    }


    let innerLayer = this.drawarea.rect(innerWidth, innerHeight)
      .addClass('ttm_svg_layer_inner').addClass('ttm_svg_layer_inner_' + layerNum).dx(innerWidthBoth / 2).dy(innerHeightTop);
    if (layerNum % 2 === 1) {
      innerLayer.addClass('ttm_svg_layer_inner_vertical');
    } else {
      innerLayer.addClass('ttm_svg_layer_inner_horizontal');
    }
    layerGroup.add(innerLayer);

    // label
    let oriText = node.nodename + ' (' + (node.treemapValues ? node.treemapValues[0] : 0) + ')';
    let titleText = this.drawarea.plain(
      oriText
    ).addClass('ttm_svg_title').addClass('ttm_svg_title_' + layerNum).dx(innerWidthBoth);
    layerGroup.add(titleText);
    titleText.y(0);

    let expandBtnPathGap = this.titlePointGap * 2;
    if (expandBtnPathGap >= 6 && innerHeightTop === 15) {
      expandBtnPathGap = 5;
    }
    let widthToShowText = outerLayer.bbox().width;
    if ((<TtreemapNode>node).children) {
      widthToShowText = outerLayer.bbox().width - innerHeightTop - expandBtnPathGap * 2;
    }
    if (titleText.bbox().width > widthToShowText) {
      let percentOfToCutText = 1.0 * (titleText.bbox().width - widthToShowText) / titleText.bbox().width;
      let numToCut = Math.ceil(percentOfToCutText * node.nodename.length) + 4; // 3 points '...'
      let shortText = '...';
      if (numToCut < node.nodename.length) {
        shortText = node.nodename.substring(0, node.nodename.length - numToCut) + '...';
      }
      titleText.plain(shortText + ' (' + (node.treemapValues ? node.treemapValues[0] : 0) + ')');
    }
    // tooltip for text 
    outerLayer.mouseover((evt: MouseEvent) => {
      this.showTooltip(node.tooltip ? node.tooltip : node.nodename, evt.clientX, evt.clientY);
    });
    outerLayer.mouseout((evt: MouseEvent) => {
      this.destroyTooltip();
    });
    titleText.mouseover((evt: MouseEvent) => {
      this.showTooltip(node.tooltip ? node.tooltip : node.nodename, evt.clientX, evt.clientY);
    });
    titleText.mouseout((evt: MouseEvent) => {
      this.destroyTooltip();
    });


    // console.log(node.nodename + ':  ' + innerHeightTop);

    // show-hide button
    if ((<TtreemapNode>node).children) {
      let ttm = this;
      // console.log(node.nodename + ':  ' + innerHeightTop + '  ' + expandBtnPathGap);
      let expandBtnGroup = this.drawarea.group().addClass('tmm_expand_btn_group').addClass('tmm_expand_btn_group_' + layerNum)
        .dx(layerGroup.x() + outerLayer.width() - innerHeightTop - expandBtnPathGap).dy(layerGroup.y());
      let expandBtnPathX1 = this.drawarea.path().addClass('tmm_expand_btn_expand').addClass('tmm_expand_btn_expand_' + layerNum)
        .plot('M' + (expandBtnPathGap) + ',' + ((innerHeightTop) / 2) + ' ' + 
        (innerHeightTop - expandBtnPathGap) + ' ,' + ((innerHeightTop) / 2));
      expandBtnGroup.add(expandBtnPathX1);
      let expandBtnPathX2 = this.drawarea.path().addClass('tmm_expand_btn_expand').addClass('tmm_expand_btn_expand_' + layerNum)
        .plot('M' + ((innerHeightTop) / 2) + ',' + (expandBtnPathGap) + ' ' + ((innerHeightTop) / 2) + ',' + (innerHeightTop - (expandBtnPathGap)));
      expandBtnGroup.add(expandBtnPathX2);
      let expandBtnCircle = this.drawarea.circle().addClass('tmm_expand_btn_circle').addClass('tmm_expand_btn_circle_' + layerNum)
        .cx((innerHeightTop) / 2).cy((innerHeightTop) / 2).radius((innerHeightTop) / 2 - this.titlePointGap)
        .click(function() {
          ttm.expandLayer(node);
        });
      expandBtnGroup.add(expandBtnCircle);

      (<TtreemapNode>node).expandButton = [expandBtnPathX1, expandBtnPathX2, expandBtnCircle, expandBtnGroup];
      (<TtreemapNode>node).expanded = true;
      const tnb = (<TtreemapNode>node).expandButton;
      tnb ? tnb[1].hide() : true;

      // clicking on node opens its under layer
      innerLayer.click(function () {
        ttm.expandLayer(node);
      });
    }
    let ret: CreateLayerReturnType = {
      innerLayer: innerLayer,
      outerLayer: outerLayer,
      layerGroup: layerGroup,
      innerHeightTop: innerHeightTop
    };
    node.layers = ret;
    if (this.maxLayerNum < layerNum) {
      this.maxLayerNum = layerNum;
    }
    node.layerNum = layerNum;

    return ret;

  }

  /**
   * 
   * 
   * @param {any} node
   */
  expandLayer (node: TtreemapNode | TtreemapLeaf) {
    if ((<TtreemapNode>node).expanded) {
      this.hideLayer(node);
    } else {
      this.showLayer(node);
    }

  }

  hideLayer (node: TtreemapNode | TtreemapLeaf) {
    // console.log(node.nodename + ': ' + node.expanded);
    if ((<TtreemapNode>node).children) {
      for (let i = 0; i < (<TtreemapNode>node).children.length; i++) {
        let child = (<TtreemapNode>node).children[i];
        child.layers ? child.layers.layerGroup.hide() : false;
        if ((<TtreemapNode>child).children) {
          const tcb = (<TtreemapNode>child).expandButton;
          tcb ? tcb[3].hide() : true;
        }
        this.hideLayer(child);
      }
    }
    (<TtreemapNode>node).expanded = false;
    if ((<TtreemapNode>node).children) {
      const tcb = (<TtreemapNode>node).expandButton;
      tcb ? tcb[1].show() : true;
    }
  }

  /**
   * always show the first layer under this node
   * 
   * @param {any} node
   */
  showLayer (node: TtreemapNode | TtreemapLeaf) {
    // console.log(node.nodename + ': ' + node.expanded);
    if ((<TtreemapNode>node).children) {
      for (let i = 0; i < (<TtreemapNode>node).children.length; i++) {
        let child = (<TtreemapNode>node).children[i];
        child.layers ? child.layers.layerGroup.show() : false;
        if ((<TtreemapNode>child).children) {
          const tcb = (<TtreemapNode>child).expandButton;
          tcb ? tcb[3].show() : true;
        }
        
      }
    }
    (<TtreemapNode>node).expanded = true;
    if ((<TtreemapNode>node).children) {
      const tcb = (<TtreemapNode>node).expandButton;
      tcb ? tcb[1].hide() : true;
    }
  }

  getMaxLayerNum () {
    return this.maxLayerNum;
  }

  showLayerUsingNum (layerNum: number, node: TtreemapNode | TtreemapLeaf) {
    
    if (typeof node === 'undefined') {
      this.hideAll();
    }
    node = typeof node !== 'undefined' ?  node : this.data;
    if (node.layerNum <= layerNum) {
      this.showLayer(node);
    }
    if ((<TtreemapNode>node).children) {
      for (let i = 0; i < (<TtreemapNode>node).children.length; i++) {
        let child = (<TtreemapNode>node).children[i];
        this.showLayerUsingNum(layerNum, child);
        
      }
    }
  }

  hideAll () {
    this.hideLayer(this.data);
  }

  showAll (node: TtreemapNode | TtreemapLeaf) {
    node = typeof node !== 'undefined' ?  node : this.data;
    this.showLayer(node);
    if ((<TtreemapNode>node).children) {
      for (let i = 0; i < (<TtreemapNode>node).children.length; i++) {
        let child = (<TtreemapNode>node).children[i];
        this.showAll(child);
        
      }
    }
  }

  setBlockCalcMethod (u: number) {
    this.calculateNodeValueBy = u;
  }

  calculateLogarithms (n: number) {
    // return Math.log(n);
    // return Math.log(n) / Math.log(10);
    // return Math.log(n) / Math.LN10;
    if (n < 10) {
      return 4;
    } else if (n < 100) {
      return 6;
    } else if (n < 1000) {
      return 8;
    } else {
      return 10;
    }

  }

  showTooltip(text: string, x: number, y: number) {
    if (this.container) {
      x = x - this.container.getBoundingClientRect().left;
      y = y - this.container.getBoundingClientRect().top;
    }
    this.tooltip = this.drawarea.text(text).attr('id', 'tmm_tooltip').x(x).y(y);
    // avoid mouse pointer
    y = y + this.tooltip.bbox().height;
    // avoid out of svg canvas
    if (x + this.tooltip.bbox().width > this.cWidth) {
      x = this.cWidth - this.tooltip.bbox().width - 4;
    }
    this.tooltipBG = this.drawarea.rect()
      .attr('id', 'tmm_tooltip_bg')
      .x(x - 2).y(y - 2)
      .attr('width', this.tooltip.bbox().width + 4)
      .attr('height', this.tooltip.bbox().height + 4);
    this.tooltip.remove();
    this.tooltip = this.drawarea.text(text).attr('id', 'tmm_tooltip').x(x).y(y);
  }

  destroyTooltip() {
    if (this.tooltip) {
      this.tooltip.remove();
    }
    if (this.tooltipBG) {
      this.tooltipBG.remove();
    }

  }
}

export interface CreateLayerReturnType {
  innerLayer: SVG.Rect;
  outerLayer: SVG.Rect;
  layerGroup: any;
  innerHeightTop: number;
}
export interface TtreemapNode {
  nodename: string;
  children: TtreemapNode[] | TtreemapLeaf[];
  expandButton?: any[];
  expanded?: boolean;
  treemapValues?: number[];
  layers?: CreateLayerReturnType;
  layerNum: number;
  tooltip: string;
}

export interface TtreemapLeaf {
  nodename: string;
  value: number;
  docIdList?: number[];
  treemapValues?: number[];
  layers?: CreateLayerReturnType;
  layerNum: number;
  tooltip: string;
}



/**
 * node data: 
 * {
 *     layername: "", 
 *     children: []
 *   }
 * 
 * leaf data: 
 * {
 *     layername: "",
 *     value: number
 * }
 */
