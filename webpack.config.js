var webpack = require('webpack');
var path = require('path');
var UglifyJSPlugin = require('uglifyjs-webpack-plugin');
var __dirname = '';

module.exports = {
  entry: './dist/test.js',
  output: {
    path: path.resolve(__dirname, 'test'),
    filename: 'test.bundle.js'
  },
  watch: true,
  devServer: {
    contentBase: path.join(__dirname, 'test'),
    compress: true,
    port: 9966,
    hot: true,
    // open: true,
    watchContentBase: true,

  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    // new UglifyJSPlugin()
  ]
};
