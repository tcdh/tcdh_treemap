/**
* TCDH Tree-Map Library
*  --  for special representation of tree-data according to the requirements of TCDH Schnitzler-Project
*
* using svg.js
*
* @version 1.0.0
* @author Li Sheng <sheng@uni-trier.de>
*
*/
define(["require", "exports", "svg.js"], function (require, exports, SVG) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    /**
     *
     *
     *
     * @class TtreemapSVG
     */
    var TtreemapSVG = /** @class */ (function () {
        function TtreemapSVG() {
            this.data = {};
            this.container = {};
            this.containerId = '';
            this.drawarea = {};
            this.tooltip = {};
            this.tooltipBG = {};
            this.leafImageUrl = '';
            this.maxLayerNum = -1;
            // setting parameters
            this.calculateNodeValueBy = 1; // 1: linear; 2: logarithms; 3: equal splitted
            this.innerHightPercentTop = 0.03;
            this.innerHightPercentBottom = 0.01;
            this.innerWidthPercentBoth = 0.01;
            this.innerWHMin = 0;
            this.titlePointGap = 3;
            this.TitleRowMin = 20;
            this.TitleRowMax = 30;
            // helping variables
            this.cWidth = 0;
            this.cHeight = 0;
            this.leafPattern = {};
            this.leafCallback = function () { };
        }
        /**
         *
         * @param containerId
         * @param data
         * @param useLogorithms
         * @param leafCallback
         * @param leafImageUrl
         */
        TtreemapSVG.prototype.init = function (containerId, data, calculateNodeValueBy, leafCallback, leafImageUrl) {
            this.calculateNodeValueBy = calculateNodeValueBy;
            this.container = document.getElementById(containerId);
            this.containerId = containerId;
            this.data = data;
            this.leafImageUrl = leafImageUrl;
            this.leafCallback = leafCallback;
            this.cWidth = this.container.clientWidth;
            this.cHeight = this.container.clientHeight;
            this.maxLayerNum = -1;
            this.recreateTreeMap();
        };
        TtreemapSVG.prototype.recreateTreeMap = function () {
            // delete all layers
            if (this.drawarea) {
                this.drawarea.remove();
            }
            this.drawarea = SVG(this.containerId).size(this.cWidth, this.cHeight).attr('id', this.containerId + '_drawarea');
            // let defs = SVG.Defs();
            // console.log(defs);
            // console.log(this.drawarea);
            var _this = this;
            if (this.leafImageUrl) {
                this.leafPattern = this.drawarea.pattern(1, 1, function (add) {
                    add.image(_this.leafImageUrl);
                }).attr('id', 'ttm_svg_leaf_image')
                    .attr('patternUnits', 'objectBoundingBox');
            }
            this.calculateNodeValue(this.data, null);
            this.createTreeMap(this.data, {
                width: this.cWidth,
                height: this.cHeight,
                x: 0,
                y: 0
            }, 0, null, null);
        };
        /**
         *
         *
         * @param {any} node
         * @returns number
         */
        TtreemapSVG.prototype.calculateNodeValue = function (node, parentNode) {
            // let oriValue = typeof values !== 'undefined' ?  values[0] : 0;
            // let normValue = typeof values !== 'undefined' ?  values[1] : 0;
            var oriValue = 0;
            var normValue = 0;
            var equalSplitValue = 1;
            var normValueFromBottom = 0;
            if (parentNode !== null) {
                equalSplitValue = 1.0 / parentNode.children.length;
            }
            if (node.children) {
                var tempOriValue = 0;
                var tempNormValue = 0;
                var tempNormValueFromBottom = 0;
                for (var i = 0; i < node.children.length; i++) {
                    var vs = this.calculateNodeValue(node.children[i], node);
                    tempOriValue += vs[0];
                    tempNormValueFromBottom += vs[2];
                }
                oriValue += tempOriValue;
                if (oriValue < 2) { // too small value
                    normValue = this.calculateLogarithms(2);
                }
                else {
                    normValue = this.calculateLogarithms(oriValue);
                }
                // console.log(node.nodename + " " + oriValue + "  " + normValue);
                normValueFromBottom += tempNormValueFromBottom;
            }
            else {
                oriValue = node.value;
                if (node.value < 2) { // too small value
                    normValue = this.calculateLogarithms(2);
                }
                else {
                    normValue = this.calculateLogarithms(node.value);
                }
                normValueFromBottom = normValue;
            }
            node.treemapValues = [oriValue, normValue, normValueFromBottom, equalSplitValue];
            return node.treemapValues;
        };
        /**
         *
         *
         * @param {any} node
         * @returns number
         */
        TtreemapSVG.prototype.calculateNodeValueOnlyOneLayer = function (node) {
            var oriValue = node.treemapValues ? node.treemapValues[0] : 0;
            var normValue = node.treemapValues ? node.treemapValues[1] : 0;
            var normValueFromNextLayer = 0;
            if (node.children) {
                var tempNormValueFromNextLayer = 0;
                for (var i = 0; i < node.children.length; i++) {
                    var vs = node.children[i].treemapValues;
                    tempNormValueFromNextLayer += vs ? vs[1] : 0;
                }
                normValueFromNextLayer += tempNormValueFromNextLayer;
            }
            else {
                normValueFromNextLayer = node.treemapValues ? node.treemapValues[2] : 0;
            }
            return [oriValue, normValueFromNextLayer];
        };
        /**
         *
         *
         * @param {any} node
         * @param {any} parentInfo
         * @param {any} layerNum
         * @param {any} parentNode
         * @param {any} lsls -- lastSiblingLayers
         * @returns array
         */
        TtreemapSVG.prototype.createTreeMap = function (node, parentInfo, layerNum, parentNode, lsls) {
            var _this_1 = this;
            var parentWidth = parentInfo.width;
            var parentHeight = parentInfo.height;
            var parentX = parentInfo.x;
            var parentY = parentInfo.y;
            var isVertical = false;
            if (layerNum % 2 === 1) {
                isVertical = true;
            }
            else {
                isVertical = false;
            }
            var useValueNum = 0;
            if (this.calculateNodeValueBy === 2) {
                useValueNum = 1;
            }
            if (this.calculateNodeValueBy === 3) {
                useValueNum = 3;
            }
            var parentValue = parentNode !== null ? this.calculateNodeValueOnlyOneLayer(parentNode)[useValueNum] : this.calculateNodeValueOnlyOneLayer(node)[useValueNum];
            var parentValueList = parentNode !== null ? this.calculateNodeValueOnlyOneLayer(parentNode) : this.calculateNodeValueOnlyOneLayer(node);
            var x = 0, y = 0;
            if (isVertical) {
                x = lsls !== null ? (lsls.layerGroup.x() + lsls.outerLayer.width()) : parentX;
                y = parentY;
            }
            else {
                x = parentX;
                y = lsls !== null ? (lsls.layerGroup.y() + lsls.outerLayer.height()) : parentY;
            }
            // calculate node width and height
            var nodeValue = node.treemapValues ? node.treemapValues[useValueNum] : 0;
            var nodeWidth = 0, nodeHeight = 0;
            if (isVertical) {
                nodeWidth = 1.0 * nodeValue / parentValue * parentWidth;
                if (this.calculateNodeValueBy === 3) {
                    nodeWidth = parentWidth * nodeValue;
                }
                nodeHeight = parentHeight;
            }
            else {
                nodeWidth = parentWidth;
                nodeHeight = 1.0 * nodeValue / parentValue * parentHeight;
                if (this.calculateNodeValueBy === 3) {
                    nodeHeight = parentHeight * nodeValue;
                }
            }
            if (!parentNode) {
                nodeHeight = parentHeight;
            }
            // if (node.nodename === 'Drucke' || node.nodename ==='Eigenadaptionen') {
            //   console.log(parentValueList);
            //   console.log(node.nodename + '  ' + nodeValue + '  ' + parentValue);
            //   console.log(node.nodename + '  ' + isVertical);
            //   console.log('parentWidth + parentHeight:  ' + parentWidth + '  ' + parentHeight);
            //   console.log('parentX+ parentY:  '+parentX + '  ' + parentY);
            //   console.log('x + y:  '+ x + '  ' + y);
            //   console.log(nodeHeight);
            //   console.log('------------------------------');
            // }
            // create top layer
            var inOutLayers = this.createLayer(node, layerNum, nodeWidth, nodeHeight, x, y);
            var innerLayer = inOutLayers.innerLayer;
            var outerLayer = inOutLayers.outerLayer;
            var layerGroup = inOutLayers.layerGroup;
            var innerHeightTop = inOutLayers.innerHeightTop;
            // console.log(node.nodename + ' nodeWidth + nodeHeight:  ' + nodeWidth + '    '+ nodeHeight);
            // console.log(node.nodename + ' innerLayer.width()+ innerLayer.height():  ' + innerLayer.width() + '    '+ innerLayer.height());
            // console.log(node.nodename + ' innerLayer.x()+ innerLayer.y():  ' + innerLayer.x() + '    '+ innerLayer.y());
            // console.log(node.nodename + ' layerGroup.x()+ layerGroup.y():  ' + layerGroup.x() + '    '+ layerGroup.y());
            // console.log('');
            // check if child exists
            if (node.children) {
                var lastSiblingLayers = null;
                // let lastSiblingInnerLayer = null;
                // let lastSiblingOuterLayer = null;
                // let lastSiblingLayerGroup = null;
                for (var i = 0; i < node.children.length; i++) {
                    var child = node.children[i];
                    lastSiblingLayers = this.createTreeMap(child, {
                        width: innerLayer.width(),
                        height: innerLayer.height(),
                        x: layerGroup.x() + (outerLayer.width() - innerLayer.width()) / 2,
                        y: layerGroup.y() + innerHeightTop
                    }, layerNum + 1, node, lastSiblingLayers);
                    // lastSiblingInnerLayer = lastSiblingLayers[0];
                    // lastSiblingOuterLayer = lastSiblingLayers[1];
                    // lastSiblingLayerGroup = lastSiblingLayers[2];
                    // console.log(child.nodename + ' lastSiblingLayer:  ' + lastSiblingOuterLayer.width() + '  ' + lastSiblingOuterLayer.height());
                    // console.log(child.nodename + ' lastSiblingLayer:  ' + lastSiblingLayerGroup.x() + '  ' + lastSiblingLayerGroup.y());
                    // console.log('');
                }
            }
            else {
                // add class innerlayer_last for leaf
                // inOutLayers[0].addClass('ttm_svg_layer_inner_last');
                var docIds_1 = node.docIdList;
                inOutLayers.innerLayer.click(function (evt) {
                    _this_1.leafCallback(evt, docIds_1);
                });
                if (this.leafImageUrl) {
                    inOutLayers.innerLayer.attr('fill', 'url(#ttm_svg_leaf_image)');
                }
            }
            return inOutLayers;
        };
        /**
         *
         *
         * @param {any} node
         * @param {any} layerNum
         * @param {any} layerWidth
         * @param {any} layerHeight
         * @param {any} x
         * @param {any} y
         * @returns array
         */
        TtreemapSVG.prototype.createLayer = function (node, layerNum, layerWidth, layerHeight, x, y) {
            var _this_1 = this;
            var layerGroup = this.drawarea.group().addClass('ttm_svg_layerGroup_' + layerNum).dx(x).dy(y);
            var outerLayer = this.drawarea.rect(layerWidth, layerHeight).addClass('ttm_svg_layer').addClass('ttm_svg_layer_' + layerNum);
            layerGroup.add(outerLayer);
            // inner rect
            var innerHeightTop = this.innerHightPercentTop * layerHeight;
            var innerHeightBottom = this.innerHightPercentBottom * layerHeight;
            var innerWidthBoth = this.innerWidthPercentBoth * layerWidth;
            if (innerHeightTop < this.TitleRowMin) {
                innerHeightTop = this.TitleRowMin;
            }
            if (innerHeightTop > this.TitleRowMax) {
                innerHeightTop = this.TitleRowMax;
            }
            if (innerHeightBottom < 5) {
                innerHeightBottom = 5;
            }
            if (innerHeightBottom > 15) {
                innerHeightBottom = 15;
            }
            if (innerWidthBoth < 5) {
                innerWidthBoth = 5;
            }
            if (innerWidthBoth > 15) {
                innerWidthBoth = 15;
            }
            // innerHeightTop = 0, innerHeightBottom = 0, innerWidthBoth = 0;
            var innerHeight = layerHeight - innerHeightTop - innerHeightBottom;
            var innerWidth = layerWidth - innerWidthBoth;
            if (innerHeight < this.innerWHMin) {
                innerHeight = this.innerWHMin;
            }
            if (innerWidth < this.innerWHMin) {
                innerWidth = this.innerWHMin;
            }
            var innerLayer = this.drawarea.rect(innerWidth, innerHeight)
                .addClass('ttm_svg_layer_inner').addClass('ttm_svg_layer_inner_' + layerNum).dx(innerWidthBoth / 2).dy(innerHeightTop);
            if (layerNum % 2 === 1) {
                innerLayer.addClass('ttm_svg_layer_inner_vertical');
            }
            else {
                innerLayer.addClass('ttm_svg_layer_inner_horizontal');
            }
            layerGroup.add(innerLayer);
            // label
            var oriText = node.nodename + ' (' + (node.treemapValues ? node.treemapValues[0] : 0) + ')';
            var titleText = this.drawarea.plain(oriText).addClass('ttm_svg_title').addClass('ttm_svg_title_' + layerNum).dx(innerWidthBoth);
            layerGroup.add(titleText);
            titleText.y(0);
            var expandBtnPathGap = this.titlePointGap * 2;
            if (expandBtnPathGap >= 6 && innerHeightTop === 15) {
                expandBtnPathGap = 5;
            }
            var widthToShowText = outerLayer.bbox().width;
            if (node.children) {
                widthToShowText = outerLayer.bbox().width - innerHeightTop - expandBtnPathGap * 2;
            }
            if (titleText.bbox().width > widthToShowText) {
                var percentOfToCutText = 1.0 * (titleText.bbox().width - widthToShowText) / titleText.bbox().width;
                var numToCut = Math.ceil(percentOfToCutText * node.nodename.length) + 4; // 3 points '...'
                var shortText = '...';
                if (numToCut < node.nodename.length) {
                    shortText = node.nodename.substring(0, node.nodename.length - numToCut) + '...';
                }
                titleText.plain(shortText + ' (' + (node.treemapValues ? node.treemapValues[0] : 0) + ')');
            }
            // tooltip for text 
            outerLayer.mouseover(function (evt) {
                _this_1.showTooltip(node.tooltip ? node.tooltip : node.nodename, evt.clientX, evt.clientY);
            });
            outerLayer.mouseout(function (evt) {
                _this_1.destroyTooltip();
            });
            titleText.mouseover(function (evt) {
                _this_1.showTooltip(node.tooltip ? node.tooltip : node.nodename, evt.clientX, evt.clientY);
            });
            titleText.mouseout(function (evt) {
                _this_1.destroyTooltip();
            });
            // console.log(node.nodename + ':  ' + innerHeightTop);
            // show-hide button
            if (node.children) {
                var ttm_1 = this;
                // console.log(node.nodename + ':  ' + innerHeightTop + '  ' + expandBtnPathGap);
                var expandBtnGroup = this.drawarea.group().addClass('tmm_expand_btn_group').addClass('tmm_expand_btn_group_' + layerNum)
                    .dx(layerGroup.x() + outerLayer.width() - innerHeightTop - expandBtnPathGap).dy(layerGroup.y());
                var expandBtnPathX1 = this.drawarea.path().addClass('tmm_expand_btn_expand').addClass('tmm_expand_btn_expand_' + layerNum)
                    .plot('M' + (expandBtnPathGap) + ',' + ((innerHeightTop) / 2) + ' ' +
                    (innerHeightTop - expandBtnPathGap) + ' ,' + ((innerHeightTop) / 2));
                expandBtnGroup.add(expandBtnPathX1);
                var expandBtnPathX2 = this.drawarea.path().addClass('tmm_expand_btn_expand').addClass('tmm_expand_btn_expand_' + layerNum)
                    .plot('M' + ((innerHeightTop) / 2) + ',' + (expandBtnPathGap) + ' ' + ((innerHeightTop) / 2) + ',' + (innerHeightTop - (expandBtnPathGap)));
                expandBtnGroup.add(expandBtnPathX2);
                var expandBtnCircle = this.drawarea.circle().addClass('tmm_expand_btn_circle').addClass('tmm_expand_btn_circle_' + layerNum)
                    .cx((innerHeightTop) / 2).cy((innerHeightTop) / 2).radius((innerHeightTop) / 2 - this.titlePointGap)
                    .click(function () {
                    ttm_1.expandLayer(node);
                });
                expandBtnGroup.add(expandBtnCircle);
                node.expandButton = [expandBtnPathX1, expandBtnPathX2, expandBtnCircle, expandBtnGroup];
                node.expanded = true;
                var tnb = node.expandButton;
                tnb ? tnb[1].hide() : true;
                // clicking on node opens its under layer
                innerLayer.click(function () {
                    ttm_1.expandLayer(node);
                });
            }
            var ret = {
                innerLayer: innerLayer,
                outerLayer: outerLayer,
                layerGroup: layerGroup,
                innerHeightTop: innerHeightTop
            };
            node.layers = ret;
            if (this.maxLayerNum < layerNum) {
                this.maxLayerNum = layerNum;
            }
            node.layerNum = layerNum;
            return ret;
        };
        /**
         *
         *
         * @param {any} node
         */
        TtreemapSVG.prototype.expandLayer = function (node) {
            if (node.expanded) {
                this.hideLayer(node);
            }
            else {
                this.showLayer(node);
            }
        };
        TtreemapSVG.prototype.hideLayer = function (node) {
            // console.log(node.nodename + ': ' + node.expanded);
            if (node.children) {
                for (var i = 0; i < node.children.length; i++) {
                    var child = node.children[i];
                    child.layers ? child.layers.layerGroup.hide() : false;
                    if (child.children) {
                        var tcb = child.expandButton;
                        tcb ? tcb[3].hide() : true;
                    }
                    this.hideLayer(child);
                }
            }
            node.expanded = false;
            if (node.children) {
                var tcb = node.expandButton;
                tcb ? tcb[1].show() : true;
            }
        };
        /**
         * always show the first layer under this node
         *
         * @param {any} node
         */
        TtreemapSVG.prototype.showLayer = function (node) {
            // console.log(node.nodename + ': ' + node.expanded);
            if (node.children) {
                for (var i = 0; i < node.children.length; i++) {
                    var child = node.children[i];
                    child.layers ? child.layers.layerGroup.show() : false;
                    if (child.children) {
                        var tcb = child.expandButton;
                        tcb ? tcb[3].show() : true;
                    }
                }
            }
            node.expanded = true;
            if (node.children) {
                var tcb = node.expandButton;
                tcb ? tcb[1].hide() : true;
            }
        };
        TtreemapSVG.prototype.getMaxLayerNum = function () {
            return this.maxLayerNum;
        };
        TtreemapSVG.prototype.showLayerUsingNum = function (layerNum, node) {
            if (typeof node === 'undefined') {
                this.hideAll();
            }
            node = typeof node !== 'undefined' ? node : this.data;
            if (node.layerNum <= layerNum) {
                this.showLayer(node);
            }
            if (node.children) {
                for (var i = 0; i < node.children.length; i++) {
                    var child = node.children[i];
                    this.showLayerUsingNum(layerNum, child);
                }
            }
        };
        TtreemapSVG.prototype.hideAll = function () {
            this.hideLayer(this.data);
        };
        TtreemapSVG.prototype.showAll = function (node) {
            node = typeof node !== 'undefined' ? node : this.data;
            this.showLayer(node);
            if (node.children) {
                for (var i = 0; i < node.children.length; i++) {
                    var child = node.children[i];
                    this.showAll(child);
                }
            }
        };
        TtreemapSVG.prototype.setBlockCalcMethod = function (u) {
            this.calculateNodeValueBy = u;
        };
        TtreemapSVG.prototype.calculateLogarithms = function (n) {
            // return Math.log(n);
            // return Math.log(n) / Math.log(10);
            // return Math.log(n) / Math.LN10;
            if (n < 10) {
                return 4;
            }
            else if (n < 100) {
                return 6;
            }
            else if (n < 1000) {
                return 8;
            }
            else {
                return 10;
            }
        };
        TtreemapSVG.prototype.showTooltip = function (text, x, y) {
            if (this.container) {
                x = x - this.container.getBoundingClientRect().left;
                y = y - this.container.getBoundingClientRect().top;
            }
            this.tooltip = this.drawarea.text(text).attr('id', 'tmm_tooltip').x(x).y(y);
            // avoid mouse pointer
            y = y + this.tooltip.bbox().height;
            // avoid out of svg canvas
            if (x + this.tooltip.bbox().width > this.cWidth) {
                x = this.cWidth - this.tooltip.bbox().width - 4;
            }
            this.tooltipBG = this.drawarea.rect()
                .attr('id', 'tmm_tooltip_bg')
                .x(x - 2).y(y - 2)
                .attr('width', this.tooltip.bbox().width + 4)
                .attr('height', this.tooltip.bbox().height + 4);
            this.tooltip.remove();
            this.tooltip = this.drawarea.text(text).attr('id', 'tmm_tooltip').x(x).y(y);
        };
        TtreemapSVG.prototype.destroyTooltip = function () {
            if (this.tooltip) {
                this.tooltip.remove();
            }
            if (this.tooltipBG) {
                this.tooltipBG.remove();
            }
        };
        return TtreemapSVG;
    }());
    exports.TtreemapSVG = TtreemapSVG;
});
/**
 * node data:
 * {
 *     layername: "",
 *     children: []
 *   }
 *
 * leaf data:
 * {
 *     layername: "",
 *     value: number
 * }
 */
//# sourceMappingURL=t-treemap-svg.js.map