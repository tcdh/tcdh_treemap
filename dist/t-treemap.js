/**
* TCDH Tree-Map Library
*  --  for special representation of tree-data according to the requirements of TCDH Schnitzler-Project
*
* using svg.js
*
* @version 1.0.0
* @author Li Sheng <sheng@uni-trier.de>
*
*/
define(["require", "exports", "./t-treemap-div", "./t-treemap-svg"], function (require, exports, t_treemap_div_1, t_treemap_svg_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    /**
     *
     *
     *
     * @class Ttreemap
     */
    var Ttreemap = /** @class */ (function () {
        function Ttreemap() {
        }
        /**
         *
         * @param containerId
         * @param data
         * @param useLogorithms
         * @param leafCallback
         * @param leafImageUrl
         */
        Ttreemap.init = function (type, containerId, data, calculateNodeValueBy, leafCallback, leafImageUrl) {
            if (type === 'svg') {
                var obj = new t_treemap_svg_1.TtreemapSVG();
                obj.init(containerId, data, calculateNodeValueBy, leafCallback, leafImageUrl);
                return obj;
            }
            else if (type === 'div') {
                var obj = new t_treemap_div_1.TtreemapDiv();
                obj.leafNumberAppendText = this.leafNumberAppendText;
                obj.init(containerId, data, calculateNodeValueBy, leafCallback, leafImageUrl);
                return obj;
            }
            return null;
        };
        Ttreemap.leafNumberAppendText = '';
        return Ttreemap;
    }());
    exports.Ttreemap = Ttreemap;
});
/**
 * node data:
 * {
 *     layername: "",
 *     children: []
 *   }
 *
 * leaf data:
 * {
 *     layername: "",
 *     value: number
 * }
 */
//# sourceMappingURL=t-treemap.js.map