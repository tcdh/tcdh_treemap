/**
* TCDH Tree-Map Library
*  --  for special representation of tree-data according to the requirements of TCDH Schnitzler-Project
*
* based on jquery 3.2
*
* @version 1.0.0
* @author Li Sheng <sheng@uni-trier.de>
*
*/
import { TtreemapType } from './t-treemap';
/**
 *
 *
 * @export
 * @class TtreemapDiv
 * @implements {TtreemapType}
 */
export declare class TtreemapDiv implements TtreemapType {
    data: TtreemapNode;
    container: JQuery<HTMLElement>;
    containerId: string;
    drawarea: JQuery<HTMLElement>;
    leafImageUrl: string;
    maxLayerNum: number;
    calculateNodeValueBy: number;
    nodeInnerLayerMargin: number;
    innerWHMin: number;
    cWidth: number | undefined;
    cHeight: number | undefined;
    TitleRowMax: number;
    leafNumberAppendText: string;
    leafCallback: (evt: any, docIds: any) => void;
    /**
     *
     *
     * @param {string} containerId
     * @param {TtreemapNode} data
     * @param {number} calculateNodeValueBy
     * @param {(evt: Event, docIds: any) => void} leafCallback
     * @param {string} leafImageUrl
     * @memberof TtreemapDiv
     */
    init(containerId: string, data: TtreemapNode, calculateNodeValueBy: number, leafCallback: (evt: Event, docIds: any) => void, leafImageUrl: string): void;
    /**
     *
     *
     * @memberof TtreemapDiv
     */
    recreateTreeMap(): void;
    /**
     *
     *
     * @param {(TtreemapNode | TtreemapLeaf)} node
     * @param {(TtreemapNode | null)} parentNode
     * @param {number} pos
     * @returns
     * @memberof TtreemapDiv
     */
    calculateNodeValue(node: TtreemapNode | TtreemapLeaf, parentNode: TtreemapNode | null, pos: number): number[];
    /**
     *
     *
     * @param {(TtreemapNode | TtreemapLeaf)} node
     * @returns
     * @memberof TtreemapDiv
     */
    calculateNodeValueOnlyOneLayer(node: TtreemapNode | TtreemapLeaf): number[];
    /**
     *
     *
     * @param {(TtreemapNode | TtreemapLeaf)} node
     * @memberof TtreemapDiv
     */
    checkAndAdjustSize(node: TtreemapNode | TtreemapLeaf): void;
    /**
     *
     *
     * @param {(TtreemapNode | TtreemapLeaf)} node
     * @param {JQuery<HTMLElement>} parentDiv
     * @param {*} parentInfo
     * @param {number} layerNum
     * @param {(TtreemapNode | TtreemapLeaf | null)} parentNode
     * @param {(CreateLayerReturnType|null)} lsls
     * @returns {CreateLayerReturnType}
     * @memberof TtreemapDiv
     */
    createTreeMap(node: TtreemapNode | TtreemapLeaf, parentDiv: JQuery<HTMLElement>, parentInfo: any, layerNum: number, parentNode: TtreemapNode | TtreemapLeaf | null, lsls: CreateLayerReturnType | null): CreateLayerReturnType;
    /**
     *
     *
     * @param {(TtreemapNode | TtreemapLeaf)} node
     * @param {(TtreemapNode | TtreemapLeaf | null)} parentNode
     * @param {JQuery<HTMLElement>} parentDiv
     * @param {number} layerNum
     * @param {number} layerWidth
     * @param {number} layerHeight
     * @param {number} marginTop
     * @param {number} marginLeft
     * @returns {CreateLayerReturnType}
     * @memberof TtreemapDiv
     */
    createLayer(node: TtreemapNode | TtreemapLeaf, parentNode: TtreemapNode | TtreemapLeaf | null, parentDiv: JQuery<HTMLElement>, layerNum: number, layerWidth: number, layerHeight: number, marginTop: number, marginLeft: number): CreateLayerReturnType;
    /**
     *
     *
     * @param {(TtreemapNode | TtreemapLeaf)} node
     * @memberof TtreemapDiv
     */
    expandLayer(node: TtreemapNode | TtreemapLeaf): void;
    /**
     *
     *
     * @param {(TtreemapNode | TtreemapLeaf)} node
     * @memberof TtreemapDiv
     */
    hideLayer(node: TtreemapNode | TtreemapLeaf): void;
    /**
     * always show the first layer under this node
     *
     * @param {(TtreemapNode | TtreemapLeaf)} node
     * @memberof TtreemapDiv
     */
    showLayer(node: TtreemapNode | TtreemapLeaf): void;
    /**
     *
     *
     * @returns
     * @memberof TtreemapDiv
     */
    getMaxLayerNum(): number;
    /**
     *
     *
     * @param {number} layerNum
     * @param {(TtreemapNode | TtreemapLeaf)} node
     * @memberof TtreemapDiv
     */
    showLayerUsingNum(layerNum: number, node: TtreemapNode | TtreemapLeaf): void;
    /**
     *
     *
     * @memberof TtreemapDiv
     */
    hideAll(): void;
    /**
     *
     *
     * @param {(TtreemapNode | TtreemapLeaf)} node
     * @memberof TtreemapDiv
     */
    showAll(node: TtreemapNode | TtreemapLeaf): void;
    /**
     *
     *
     * @param {number} u
     * @memberof TtreemapDiv
     */
    setBlockCalcMethod(u: number): void;
    /**
     *
     *
     * @param {number} n
     * @returns
     * @memberof TtreemapDiv
     */
    calculateLogarithms(n: number): 4 | 6 | 8 | 10;
    /**
     *
     *
     * @private
     * @param {(JQuery<HTMLElement> | undefined)} outerLayer
     * @returns
     * @memberof TtreemapDiv
     */
    private getLayerWidth;
    replaceBRtags(str: string): string;
}
export interface CreateLayerReturnType {
    innerLayer: JQuery<HTMLElement>;
    titleDiv: JQuery<HTMLElement>;
    titleSpan: JQuery<HTMLElement>;
    outerLayer: JQuery<HTMLElement>;
    innerTitleText: JQuery<HTMLElement>;
}
export interface TtreemapNode {
    nodename: string;
    pos: number;
    children: TtreemapNode[] | TtreemapLeaf[];
    expandButton?: JQuery<HTMLElement>;
    expanded?: boolean;
    treemapValues?: number[];
    layers: CreateLayerReturnType;
    layerNum: number;
    tooltip: string;
    showValue: boolean;
}
export interface TtreemapLeaf {
    nodename: string;
    pos: number;
    value: number;
    docIdList?: number[];
    treemapValues?: number[];
    layers: CreateLayerReturnType;
    firstId: number;
    layerNum: number;
    tooltip: string;
    backgroundImageUrl?: string;
    showValue: boolean;
}
/**
 * node data:
 * {
 *     layername: "",
 *     children: []
 *   }
 *
 * leaf data:
 * {
 *     layername: "",
 *     value: number
 * }
 */
