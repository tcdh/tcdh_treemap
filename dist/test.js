define(["require", "exports", "./t-treemap"], function (require, exports, t_treemap_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    window.data1 = {
        nodename: 'n1',
        tooltip: 'n1',
        children: [
            {
                nodename: 'n2',
                tooltip: 'n2',
                value: 3
            },
            {
                nodename: 'n3',
                tooltip: 'n3',
                children: [
                    {
                        nodename: 'n11',
                        tooltip: 'n11',
                        value: 3
                    },
                    {
                        nodename: 'n12',
                        tooltip: 'n12',
                        value: 1
                    },
                    {
                        nodename: 'n13',
                        tooltip: 'n13',
                        value: 2
                    }
                ]
            },
            {
                nodename: 'n4',
                tooltip: 'n4',
                children: [
                    {
                        nodename: 'n5',
                        tooltip: 'n5',
                        value: 3
                    },
                    {
                        nodename: 'n6',
                        tooltip: 'n6',
                        children: [
                            {
                                nodename: 'n8',
                                tooltip: 'n8',
                                value: 2
                            },
                            {
                                nodename: 'n9',
                                tooltip: 'n9',
                                value: 3
                            },
                            {
                                nodename: 'n10',
                                tooltip: 'n10',
                                children: [
                                    {
                                        nodename: 'n14',
                                        tooltip: 'n14',
                                        value: 2
                                    },
                                    {
                                        nodename: 'n15',
                                        tooltip: 'n15',
                                        value: 3
                                    },
                                    {
                                        nodename: 'n16',
                                        tooltip: 'n16',
                                        value: 5
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        nodename: 'n7',
                        tooltip: 'n7',
                        value: 3
                    }
                ]
            }
        ]
    };
    window.data2 = {
        nodename: 'n1',
        tooltip: 'n1',
        children: [
            {
                nodename: 'n2',
                tooltip: 'n2',
                value: 200
            },
            {
                nodename: 'n3',
                tooltip: 'n3',
                children: [
                    {
                        nodename: 'n11',
                        tooltip: 'n11',
                        value: 3
                    },
                    {
                        nodename: 'n12',
                        tooltip: 'n12',
                        value: 1
                    },
                    {
                        nodename: 'n13',
                        tooltip: 'n13',
                        value: 2
                    }
                ]
            },
            {
                nodename: 'n4',
                tooltip: 'n4',
                children: [
                    {
                        nodename: 'n5',
                        tooltip: 'n5',
                        value: 3
                    },
                    {
                        nodename: 'n6',
                        tooltip: 'n6',
                        children: [
                            {
                                nodename: 'n8',
                                tooltip: 'n8',
                                value: 2
                            },
                            {
                                nodename: 'n9',
                                tooltip: 'n9',
                                value: 400
                            },
                            {
                                nodename: 'n10',
                                tooltip: 'n10',
                                children: [
                                    {
                                        nodename: 'n14',
                                        tooltip: 'n14',
                                        value: 2
                                    },
                                    {
                                        nodename: 'n15',
                                        tooltip: 'n15',
                                        value: 3
                                    },
                                    {
                                        nodename: 'n16',
                                        tooltip: 'n16',
                                        value: 5
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        nodename: 'n7',
                        tooltip: 'n7',
                        value: 3
                    }
                ]
            }
        ]
    };
    window.data3 = {
        'nodename': 'Fräulein Else',
        'children': [
            {
                'nodename': 'Werkniederschriften',
                'tooltip': 'Werkniederschriften',
                'children': [
                    {
                        'nodename': 'Manuskripte',
                        'tooltip': 'Manuskripte',
                        'children': [
                            {
                                'nodename': 'hsl. Werkniederschrift',
                                'tooltip': 'hsl. Werkniederschrift',
                                'value': 316
                            }
                        ]
                    },
                    {
                        'nodename': 'Typoskripte',
                        'tooltip': 'Typoskripte',
                        'children': [
                            {
                                'nodename': 'msl. Werkniederschrift',
                                'tooltip': 'msl. Werkniederschrift',
                                'value': 188
                            }
                        ]
                    },
                    {
                        'nodename': 'hsl. Werkniederschrift',
                        'tooltip': 'hsl. Werkniederschrift',
                        'value': 2
                    }
                ]
            },
            {
                'nodename': 'Skizzen',
                'tooltip': 'Skizzen',
                'children': [
                    {
                        'nodename': 'msl. Skizze 1',
                        'tooltip': 'msl. Skizze 1',
                        'value': 2
                    },
                    {
                        'nodename': 'msl. Skizze 2',
                        'tooltip': 'msl. Skizze 2',
                        'value': 6
                    }
                ]
            },
            {
                'nodename': 'Paralipomena <br/>und andere Materialien',
                'tooltip': 'Paralipomena <br/>und andere Materialien',
                'children': [
                    {
                        'nodename': 'Paralipomena I',
                        'tooltip': 'Paralipomena I',
                        'children': [
                            {
                                'nodename': 'Typoskripte',
                                'tooltip': 'Typoskripte',
                                'children': [
                                    {
                                        'nodename': 'msl. Stoffnotiz 3',
                                        'tooltip': 'msl. Stoffnotiz 3',
                                        'value': 8
                                    },
                                    {
                                        'nodename': 'msl. Stoffnotiz 4',
                                        'tooltip': 'msl. Stoffnotiz 4',
                                        'value': 2
                                    },
                                    {
                                        'nodename': 'msl. Stoffnotiz 5',
                                        'tooltip': 'msl. Stoffnotiz 5',
                                        'value': 8
                                    },
                                    {
                                        'nodename': 'msl. Stoffnotiz 2',
                                        'tooltip': 'msl. Stoffnotiz 2',
                                        'value': 4
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        'nodename': 'Paralipomena III',
                        'tooltip': 'Paralipomena III',
                        'children': [
                            {
                                'nodename': 'Manuskripte',
                                'tooltip': 'Manuskripte',
                                'children': [
                                    {
                                        'nodename': 'hsl. Figurenverzeichnis',
                                        'tooltip': 'hsl. Figurenverzeichnis',
                                        'value': 2
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    };
    window.data4 = {
        'nodename': 'Fräulein Else',
        'tooltip': 'Fräulein Else',
        'children': [
            {
                'nodename': 'Werkniederschriften',
                'tooltip': 'Werkniederschriften',
                'children': [
                    {
                        'nodename': 'Manuskripte',
                        'tooltip': 'Manuskripte',
                        'children': [
                            {
                                'nodename': 'H1',
                                'tooltip': 'hsl. Werkniederschrift',
                                'value': 318,
                                'docIdList': [
                                    10,
                                    11,
                                    12,
                                    13,
                                    14,
                                    15,
                                    16,
                                    17,
                                    18,
                                    19,
                                    20,
                                    21,
                                    22,
                                    23,
                                    24,
                                    25,
                                    26,
                                    27,
                                    28,
                                    29,
                                    30,
                                    31,
                                    32,
                                    33,
                                    34,
                                    35,
                                    36,
                                    37,
                                    38,
                                    39,
                                    40,
                                    41,
                                    42,
                                    43,
                                    44,
                                    45,
                                    46,
                                    47,
                                    48,
                                    49,
                                    50,
                                    51,
                                    52,
                                    53,
                                    54,
                                    55,
                                    56,
                                    57,
                                    58,
                                    59,
                                    60,
                                    61,
                                    62,
                                    63,
                                    64,
                                    65,
                                    66,
                                    67,
                                    68,
                                    69,
                                    70,
                                    71,
                                    72,
                                    73,
                                    74,
                                    75,
                                    76,
                                    77,
                                    78,
                                    79,
                                    80,
                                    81,
                                    82,
                                    83,
                                    84,
                                    85,
                                    86,
                                    87,
                                    88,
                                    89,
                                    90,
                                    91,
                                    92,
                                    93,
                                    94,
                                    95,
                                    96,
                                    97,
                                    98,
                                    99,
                                    100,
                                    101,
                                    102,
                                    103,
                                    104,
                                    105,
                                    106,
                                    107,
                                    108,
                                    109,
                                    110,
                                    111,
                                    112,
                                    113,
                                    114,
                                    115,
                                    116,
                                    117,
                                    118,
                                    119,
                                    120,
                                    121,
                                    122,
                                    123,
                                    124,
                                    125,
                                    126,
                                    127,
                                    128,
                                    129,
                                    130,
                                    131,
                                    132,
                                    133,
                                    134,
                                    135,
                                    136,
                                    137,
                                    138,
                                    139,
                                    140,
                                    141,
                                    142,
                                    143,
                                    144,
                                    145,
                                    146,
                                    147,
                                    148,
                                    149,
                                    150,
                                    151,
                                    152,
                                    153,
                                    154,
                                    155,
                                    156,
                                    157,
                                    158,
                                    159,
                                    160,
                                    161,
                                    162,
                                    163,
                                    164,
                                    165,
                                    166,
                                    167,
                                    168,
                                    169,
                                    170,
                                    171,
                                    172,
                                    173,
                                    174,
                                    175,
                                    176,
                                    177,
                                    178,
                                    179,
                                    180,
                                    181,
                                    182,
                                    183,
                                    184,
                                    185,
                                    186,
                                    187,
                                    188,
                                    189,
                                    190,
                                    191,
                                    192,
                                    193,
                                    194,
                                    195,
                                    196,
                                    197,
                                    198,
                                    199,
                                    200,
                                    201,
                                    202,
                                    203,
                                    204,
                                    205,
                                    206,
                                    207,
                                    208,
                                    209,
                                    210,
                                    211,
                                    212,
                                    213,
                                    214,
                                    215,
                                    216,
                                    217,
                                    218,
                                    219,
                                    220,
                                    221,
                                    222,
                                    223,
                                    224,
                                    225,
                                    226,
                                    227,
                                    228,
                                    229,
                                    230,
                                    231,
                                    232,
                                    233,
                                    234,
                                    235,
                                    236,
                                    237,
                                    238,
                                    239,
                                    240,
                                    241,
                                    242,
                                    243,
                                    244,
                                    245,
                                    246,
                                    247,
                                    248,
                                    249,
                                    250,
                                    251,
                                    252,
                                    253,
                                    254,
                                    255,
                                    256,
                                    257,
                                    258,
                                    259,
                                    260,
                                    261,
                                    262,
                                    263,
                                    264,
                                    265,
                                    266,
                                    267,
                                    268,
                                    269,
                                    270,
                                    271,
                                    272,
                                    273,
                                    274,
                                    275,
                                    276,
                                    277,
                                    278,
                                    279,
                                    280,
                                    281,
                                    282,
                                    283,
                                    284,
                                    285,
                                    286,
                                    287,
                                    288,
                                    289,
                                    290,
                                    291,
                                    292,
                                    293,
                                    294,
                                    295,
                                    296,
                                    297,
                                    298,
                                    299,
                                    300,
                                    301,
                                    302,
                                    303,
                                    304,
                                    305,
                                    306,
                                    307,
                                    308,
                                    309,
                                    310,
                                    311,
                                    312,
                                    313,
                                    314,
                                    315,
                                    316,
                                    317,
                                    318,
                                    319,
                                    320,
                                    321,
                                    322,
                                    323,
                                    324,
                                    325,
                                    9258,
                                    9259
                                ],
                                'asdtextsortenteilsigle': 'H',
                                'asdmaterialteilsigle': null,
                                'asdtextsortennummer': '1'
                            }
                        ]
                    },
                    {
                        'nodename': 'Typoskripte',
                        'tooltip': 'Typoskripte',
                        'children': [
                            {
                                'nodename': 'T1',
                                'tooltip': 'msl. Werkniederschrift',
                                'value': 188,
                                'docIdList': [
                                    326,
                                    327,
                                    336,
                                    337,
                                    338,
                                    339,
                                    340,
                                    341,
                                    342,
                                    343,
                                    344,
                                    345,
                                    346,
                                    347,
                                    348,
                                    349,
                                    350,
                                    351,
                                    352,
                                    353,
                                    354,
                                    355,
                                    356,
                                    357,
                                    358,
                                    359,
                                    360,
                                    361,
                                    362,
                                    363,
                                    364,
                                    365,
                                    366,
                                    367,
                                    368,
                                    369,
                                    370,
                                    371,
                                    372,
                                    373,
                                    374,
                                    375,
                                    376,
                                    377,
                                    378,
                                    379,
                                    380,
                                    381,
                                    382,
                                    383,
                                    384,
                                    385,
                                    386,
                                    387,
                                    388,
                                    389,
                                    390,
                                    391,
                                    392,
                                    393,
                                    394,
                                    395,
                                    396,
                                    397,
                                    398,
                                    399,
                                    400,
                                    401,
                                    402,
                                    403,
                                    404,
                                    405,
                                    406,
                                    407,
                                    408,
                                    409,
                                    410,
                                    411,
                                    412,
                                    413,
                                    414,
                                    415,
                                    416,
                                    417,
                                    418,
                                    419,
                                    420,
                                    421,
                                    422,
                                    423,
                                    424,
                                    425,
                                    426,
                                    427,
                                    428,
                                    429,
                                    430,
                                    431,
                                    432,
                                    433,
                                    434,
                                    435,
                                    436,
                                    437,
                                    438,
                                    439,
                                    440,
                                    441,
                                    442,
                                    443,
                                    444,
                                    445,
                                    446,
                                    447,
                                    448,
                                    449,
                                    450,
                                    451,
                                    452,
                                    453,
                                    454,
                                    455,
                                    456,
                                    457,
                                    458,
                                    459,
                                    460,
                                    461,
                                    462,
                                    463,
                                    464,
                                    465,
                                    466,
                                    467,
                                    468,
                                    469,
                                    470,
                                    471,
                                    472,
                                    473,
                                    474,
                                    475,
                                    476,
                                    477,
                                    478,
                                    479,
                                    480,
                                    481,
                                    482,
                                    483,
                                    484,
                                    485,
                                    486,
                                    487,
                                    488,
                                    489,
                                    490,
                                    491,
                                    492,
                                    493,
                                    494,
                                    495,
                                    496,
                                    497,
                                    498,
                                    499,
                                    500,
                                    501,
                                    502,
                                    503,
                                    504,
                                    505,
                                    506,
                                    507,
                                    508,
                                    509,
                                    8378,
                                    8379,
                                    8380,
                                    8381,
                                    8382,
                                    8383,
                                    8384,
                                    8385,
                                    8386,
                                    8387,
                                    8388,
                                    8389
                                ],
                                'asdtextsortenteilsigle': 'T',
                                'asdmaterialteilsigle': null,
                                'asdtextsortennummer': '1'
                            },
                            {
                                'nodename': '*T2',
                                'tooltip': 'erschl. msl. Verlagsmanuskript (Zsolnay)',
                                'value': 1,
                                'docIdList': [
                                    10125
                                ],
                                'asdtextsortenteilsigle': '*T',
                                'asdmaterialteilsigle': null,
                                'asdtextsortennummer': '2'
                            }
                        ]
                    }
                ]
            },
            {
                'nodename': 'Skizzen',
                'tooltip': 'Skizzen',
                'children': [
                    {
                        'nodename': 'ST1',
                        'tooltip': 'msl. Skizze 1',
                        'value': 2,
                        'docIdList': [
                            328,
                            329
                        ],
                        'asdtextsortenteilsigle': 'S',
                        'asdmaterialteilsigle': 'T',
                        'asdtextsortennummer': '1'
                    },
                    {
                        'nodename': 'ST2',
                        'tooltip': 'msl. Skizze 2',
                        'value': 6,
                        'docIdList': [
                            330,
                            331,
                            332,
                            333,
                            334,
                            335
                        ],
                        'asdtextsortenteilsigle': 'S',
                        'asdmaterialteilsigle': 'T',
                        'asdtextsortennummer': '2'
                    }
                ]
            },
            {
                'nodename': 'Paralipomena <br/>und andere Materialien',
                'tooltip': 'Paralipomena <br/>und andere Materialien',
                'children': [
                    {
                        'nodename': 'Paralipomena',
                        'tooltip': 'Paralipomena',
                        'children': [
                            {
                                'nodename': 'Manuskripte',
                                'tooltip': 'Manuskripte',
                                'children': [
                                    {
                                        'nodename': 'M2',
                                        'tooltip': 'hsl. Stoffnotiz 1',
                                        'value': 2,
                                        'docIdList': [
                                            5027,
                                            5028
                                        ],
                                        'asdtextsortenteilsigle': 'M',
                                        'asdmaterialteilsigle': null,
                                        'asdtextsortennummer': '2'
                                    },
                                    {
                                        'nodename': 'M2',
                                        'tooltip': 'hsl. Stoffnotiz 2',
                                        'value': 2,
                                        'docIdList': [
                                            5037,
                                            5038
                                        ],
                                        'asdtextsortenteilsigle': 'M',
                                        'asdmaterialteilsigle': null,
                                        'asdtextsortennummer': '2'
                                    },
                                    {
                                        'nodename': 'PH14',
                                        'tooltip': 'hsl. Figurenverzeichnis',
                                        'value': 2,
                                        'docIdList': [
                                            8558,
                                            8559
                                        ],
                                        'asdtextsortenteilsigle': 'P',
                                        'asdmaterialteilsigle': 'H',
                                        'asdtextsortennummer': '14'
                                    },
                                    {
                                        'nodename': 'PH14',
                                        'tooltip': 'Umschlag zu H.I.N. 217.366',
                                        'value': 4,
                                        'docIdList': [
                                            8560,
                                            8561,
                                            10921,
                                            10922
                                        ],
                                        'asdtextsortenteilsigle': 'P',
                                        'asdmaterialteilsigle': 'H',
                                        'asdtextsortennummer': '14'
                                    },
                                    {
                                        'nodename': '',
                                        'tooltip': 'Bauchbinde zu A257,11',
                                        'value': 2,
                                        'docIdList': [
                                            8566,
                                            8567
                                        ],
                                        'asdtextsortenteilsigle': null,
                                        'asdmaterialteilsigle': null,
                                        'asdtextsortennummer': null
                                    }
                                ]
                            },
                            {
                                'nodename': 'Typoskripte',
                                'tooltip': 'Typoskripte',
                                'children': [
                                    {
                                        'nodename': '',
                                        'tooltip': 'msl. Stoffnotiz 5',
                                        'value': 2,
                                        'docIdList': [
                                            8544,
                                            8545
                                        ],
                                        'asdtextsortenteilsigle': null,
                                        'asdmaterialteilsigle': null,
                                        'asdtextsortennummer': null
                                    },
                                    {
                                        'nodename': '',
                                        'tooltip': 'msl. Stoffnotiz 6',
                                        'value': 2,
                                        'docIdList': [
                                            8546,
                                            8547
                                        ],
                                        'asdtextsortenteilsigle': null,
                                        'asdmaterialteilsigle': null,
                                        'asdtextsortennummer': null
                                    },
                                    {
                                        'nodename': '',
                                        'tooltip': 'msl. Stoffnotiz 7',
                                        'value': 2,
                                        'docIdList': [
                                            8548,
                                            8549
                                        ],
                                        'asdtextsortenteilsigle': null,
                                        'asdmaterialteilsigle': null,
                                        'asdtextsortennummer': null
                                    },
                                    {
                                        'nodename': '',
                                        'tooltip': 'msl. Stoffnotiz 8',
                                        'value': 2,
                                        'docIdList': [
                                            8550,
                                            8551
                                        ],
                                        'asdtextsortenteilsigle': null,
                                        'asdmaterialteilsigle': null,
                                        'asdtextsortennummer': null
                                    },
                                    {
                                        'nodename': '',
                                        'tooltip': 'msl. Stoffnotiz 9',
                                        'value': 2,
                                        'docIdList': [
                                            8552,
                                            8553
                                        ],
                                        'asdtextsortenteilsigle': null,
                                        'asdmaterialteilsigle': null,
                                        'asdtextsortennummer': null
                                    },
                                    {
                                        'nodename': '',
                                        'tooltip': 'msl. Stoffnotiz 10',
                                        'value': 2,
                                        'docIdList': [
                                            8554,
                                            8555
                                        ],
                                        'asdtextsortenteilsigle': null,
                                        'asdmaterialteilsigle': null,
                                        'asdtextsortennummer': null
                                    },
                                    {
                                        'nodename': '',
                                        'tooltip': 'msl. Stoffnotiz 11',
                                        'value': 2,
                                        'docIdList': [
                                            8556,
                                            8557
                                        ],
                                        'asdtextsortenteilsigle': null,
                                        'asdmaterialteilsigle': null,
                                        'asdtextsortennummer': null
                                    },
                                    {
                                        'nodename': '',
                                        'tooltip': 'msl. Stoffnotiz 12',
                                        'value': 2,
                                        'docIdList': [
                                            9165,
                                            9166
                                        ],
                                        'asdtextsortenteilsigle': null,
                                        'asdmaterialteilsigle': null,
                                        'asdtextsortennummer': null
                                    },
                                    {
                                        'nodename': '',
                                        'tooltip': 'msl. Stoffnotiz 13',
                                        'value': 2,
                                        'docIdList': [
                                            9167,
                                            9168
                                        ],
                                        'asdtextsortenteilsigle': null,
                                        'asdmaterialteilsigle': null,
                                        'asdtextsortennummer': null
                                    },
                                    {
                                        'nodename': '',
                                        'tooltip': 'msl. Stoffnotiz 3',
                                        'value': 2,
                                        'docIdList': [
                                            9467,
                                            9468
                                        ],
                                        'asdtextsortenteilsigle': null,
                                        'asdmaterialteilsigle': null,
                                        'asdtextsortennummer': null
                                    },
                                    {
                                        'nodename': '',
                                        'tooltip': 'msl. Stoffnotiz 4',
                                        'value': 2,
                                        'docIdList': [
                                            9587,
                                            9588
                                        ],
                                        'asdtextsortenteilsigle': null,
                                        'asdmaterialteilsigle': null,
                                        'asdtextsortennummer': null
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                'nodename': 'Drucke',
                'tooltip': 'Drucke',
                'children': [
                    {
                        'nodename': 'Druckfahnen',
                        'tooltip': 'Druckfahnen',
                        'children': [
                            {
                                'nodename': '*D0',
                                'tooltip': 'erschl. Druckfahne',
                                'value': 1,
                                'docIdList': [
                                    10124
                                ],
                                'asdtextsortenteilsigle': '*D',
                                'asdmaterialteilsigle': '0',
                                'asdtextsortennummer': null
                            }
                        ]
                    },
                    {
                        'nodename': 'Bucherstdruck',
                        'tooltip': 'Bucherstdruck',
                        'children': [
                            {
                                'nodename': 'D',
                                'tooltip': 'Bucherstdruck',
                                'value': 144,
                                'docIdList': [
                                    10127,
                                    10128,
                                    10129,
                                    10130,
                                    10131,
                                    10132,
                                    10133,
                                    10134,
                                    10135,
                                    10136,
                                    10137,
                                    10138,
                                    10139,
                                    10140,
                                    10141,
                                    10142,
                                    10143,
                                    10144,
                                    10145,
                                    10146,
                                    10147,
                                    10148,
                                    10149,
                                    10150,
                                    10151,
                                    10152,
                                    10153,
                                    10154,
                                    10155,
                                    10156,
                                    10157,
                                    10158,
                                    10159,
                                    10160,
                                    10161,
                                    10162,
                                    10163,
                                    10164,
                                    10165,
                                    10166,
                                    10167,
                                    10168,
                                    10169,
                                    10170,
                                    10171,
                                    10172,
                                    10173,
                                    10174,
                                    10175,
                                    10176,
                                    10177,
                                    10178,
                                    10179,
                                    10180,
                                    10181,
                                    10182,
                                    10183,
                                    10184,
                                    10185,
                                    10186,
                                    10187,
                                    10188,
                                    10189,
                                    10190,
                                    10191,
                                    10192,
                                    10193,
                                    10194,
                                    10195,
                                    10196,
                                    10197,
                                    10198,
                                    10199,
                                    10200,
                                    10201,
                                    10202,
                                    10203,
                                    10204,
                                    10205,
                                    10206,
                                    10207,
                                    10208,
                                    10209,
                                    10210,
                                    10211,
                                    10212,
                                    10213,
                                    10214,
                                    10215,
                                    10216,
                                    10217,
                                    10218,
                                    10219,
                                    10220,
                                    10221,
                                    10222,
                                    10223,
                                    10224,
                                    10225,
                                    10226,
                                    10227,
                                    10228,
                                    10229,
                                    10230,
                                    10231,
                                    10232,
                                    10233,
                                    10234,
                                    10235,
                                    10236,
                                    10237,
                                    10238,
                                    10239,
                                    10240,
                                    10241,
                                    10242,
                                    10243,
                                    10244,
                                    10245,
                                    10246,
                                    10247,
                                    10248,
                                    10249,
                                    10250,
                                    10251,
                                    10252,
                                    10253,
                                    10254,
                                    10255,
                                    10256,
                                    10257,
                                    10258,
                                    10259,
                                    10260,
                                    10261,
                                    10262,
                                    10263,
                                    10264,
                                    10265,
                                    10266,
                                    10267,
                                    10268,
                                    10269,
                                    10270
                                ],
                                'asdtextsortenteilsigle': 'D',
                                'asdmaterialteilsigle': null,
                                'asdtextsortennummer': null
                            }
                        ]
                    },
                    {
                        'nodename': 'Journaldruck',
                        'tooltip': 'Journaldruck',
                        'children': [
                            {
                                'nodename': 'DJ',
                                'tooltip': 'Journaldruck',
                                'value': 63,
                                'docIdList': [
                                    10271,
                                    10272,
                                    10273,
                                    10274,
                                    10275,
                                    10276,
                                    10277,
                                    10278,
                                    10279,
                                    10280,
                                    10281,
                                    10282,
                                    10283,
                                    10284,
                                    10285,
                                    10286,
                                    10287,
                                    10288,
                                    10289,
                                    10290,
                                    10291,
                                    10292,
                                    10293,
                                    10294,
                                    10295,
                                    10296,
                                    10297,
                                    10298,
                                    10299,
                                    10300,
                                    10301,
                                    10302,
                                    10303,
                                    10304,
                                    10305,
                                    10306,
                                    10307,
                                    10308,
                                    10309,
                                    10310,
                                    10311,
                                    10312,
                                    10313,
                                    10314,
                                    10315,
                                    10316,
                                    10317,
                                    10318,
                                    10319,
                                    10320,
                                    10321,
                                    10322,
                                    10323,
                                    10324,
                                    10325,
                                    10326,
                                    10327,
                                    10328,
                                    10329,
                                    10330,
                                    10331,
                                    10332,
                                    10333
                                ],
                                'asdtextsortenteilsigle': 'D',
                                'asdmaterialteilsigle': 'J',
                                'asdtextsortennummer': null
                            }
                        ]
                    }
                ]
            },
            {
                'nodename': 'Eigenadaptionen',
                'tooltip': 'Eigenadaptionen',
                'children': [
                    {
                        'nodename': 'Adaptionen für die Bühne; Libretti',
                        'tooltip': 'Adaptionen für die Bühne; Libretti',
                        'children': [
                            {
                                'nodename': 'Manuskripte',
                                'tooltip': 'Manuskripte',
                                'children': [
                                    {
                                        'nodename': '*B',
                                        'tooltip': 'erschl. Bühnenbild',
                                        'value': 1,
                                        'docIdList': [
                                            10126
                                        ],
                                        'asdtextsortenteilsigle': '*B',
                                        'asdmaterialteilsigle': null,
                                        'asdtextsortennummer': null
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ]
    };
    window.data5 = {
        'nodename': 'Fräulein Else',
        'tooltip': 'Fräulein Else',
        'children': [
            {
                'nodename': 'Cambridge University Library, Arthur Schnitzler Papers',
                'tooltip': 'Cambridge University Library, Arthur Schnitzler Papers',
                'children': [
                    {
                        'nodename': 'undefined',
                        'tooltip': 'undefined',
                        'children': [
                            {
                                'nodename': 'A140',
                                'tooltip': 'A140',
                                'children': [
                                    {
                                        'nodename': '',
                                        'tooltip': 'Mappe zu A140',
                                        'value': 9,
                                        'docIdList': [
                                            1,
                                            2,
                                            3,
                                            4,
                                            5,
                                            6,
                                            7,
                                            8,
                                            9
                                        ],
                                        'asdtextsortenteilsigle': null,
                                        'asdmaterialteilsigle': null,
                                        'asdtextsortennummer': null,
                                        'ewtextsortenteilsigle': null,
                                        'ewmaterialteilsigle': null,
                                        'ewtextsortennummer': null,
                                        'layerExtendValueArray': [
                                            'Cambridge University Library, Arthur Schnitzler Papers',
                                            null,
                                            'A140',
                                            'Mappe zu A140'
                                        ],
                                        'wTeilSigle': '',
                                        'uTeilSigle': ''
                                    },
                                    {
                                        'nodename': 'H1',
                                        'tooltip': 'hsl. Werkniederschrift',
                                        'value': 316,
                                        'docIdList': [
                                            10,
                                            11,
                                            12,
                                            13,
                                            14,
                                            15,
                                            16,
                                            17,
                                            18,
                                            19,
                                            20,
                                            21,
                                            22,
                                            23,
                                            24,
                                            25,
                                            26,
                                            27,
                                            28,
                                            29,
                                            30,
                                            31,
                                            32,
                                            33,
                                            34,
                                            35,
                                            36,
                                            37,
                                            38,
                                            39,
                                            40,
                                            41,
                                            42,
                                            43,
                                            44,
                                            45,
                                            46,
                                            47,
                                            48,
                                            49,
                                            50,
                                            51,
                                            52,
                                            53,
                                            54,
                                            55,
                                            56,
                                            57,
                                            58,
                                            59,
                                            60,
                                            61,
                                            62,
                                            63,
                                            64,
                                            65,
                                            66,
                                            67,
                                            68,
                                            69,
                                            70,
                                            71,
                                            72,
                                            73,
                                            74,
                                            75,
                                            76,
                                            77,
                                            78,
                                            79,
                                            80,
                                            81,
                                            82,
                                            83,
                                            84,
                                            85,
                                            86,
                                            87,
                                            88,
                                            89,
                                            90,
                                            91,
                                            92,
                                            93,
                                            94,
                                            95,
                                            96,
                                            97,
                                            98,
                                            99,
                                            100,
                                            101,
                                            102,
                                            103,
                                            104,
                                            105,
                                            106,
                                            107,
                                            108,
                                            109,
                                            110,
                                            111,
                                            112,
                                            113,
                                            114,
                                            115,
                                            116,
                                            117,
                                            118,
                                            119,
                                            120,
                                            121,
                                            122,
                                            123,
                                            124,
                                            125,
                                            126,
                                            127,
                                            128,
                                            129,
                                            130,
                                            131,
                                            132,
                                            133,
                                            134,
                                            135,
                                            136,
                                            137,
                                            138,
                                            139,
                                            140,
                                            141,
                                            142,
                                            143,
                                            144,
                                            145,
                                            146,
                                            147,
                                            148,
                                            149,
                                            150,
                                            151,
                                            152,
                                            153,
                                            154,
                                            155,
                                            156,
                                            157,
                                            158,
                                            159,
                                            160,
                                            161,
                                            162,
                                            163,
                                            164,
                                            165,
                                            166,
                                            167,
                                            168,
                                            169,
                                            170,
                                            171,
                                            172,
                                            173,
                                            174,
                                            175,
                                            176,
                                            177,
                                            178,
                                            179,
                                            180,
                                            181,
                                            182,
                                            183,
                                            184,
                                            185,
                                            186,
                                            187,
                                            188,
                                            189,
                                            190,
                                            191,
                                            192,
                                            193,
                                            194,
                                            195,
                                            196,
                                            197,
                                            198,
                                            199,
                                            200,
                                            201,
                                            202,
                                            203,
                                            204,
                                            205,
                                            206,
                                            207,
                                            208,
                                            209,
                                            210,
                                            211,
                                            212,
                                            213,
                                            214,
                                            215,
                                            216,
                                            217,
                                            218,
                                            219,
                                            220,
                                            221,
                                            222,
                                            223,
                                            224,
                                            225,
                                            226,
                                            227,
                                            228,
                                            229,
                                            230,
                                            231,
                                            232,
                                            233,
                                            234,
                                            235,
                                            236,
                                            237,
                                            238,
                                            239,
                                            240,
                                            241,
                                            242,
                                            243,
                                            244,
                                            245,
                                            246,
                                            247,
                                            248,
                                            249,
                                            250,
                                            251,
                                            252,
                                            253,
                                            254,
                                            255,
                                            256,
                                            257,
                                            258,
                                            259,
                                            260,
                                            261,
                                            262,
                                            263,
                                            264,
                                            265,
                                            266,
                                            267,
                                            268,
                                            269,
                                            270,
                                            271,
                                            272,
                                            273,
                                            274,
                                            275,
                                            276,
                                            277,
                                            278,
                                            279,
                                            280,
                                            281,
                                            282,
                                            283,
                                            284,
                                            285,
                                            286,
                                            287,
                                            288,
                                            289,
                                            290,
                                            291,
                                            292,
                                            293,
                                            294,
                                            295,
                                            296,
                                            297,
                                            298,
                                            299,
                                            300,
                                            301,
                                            302,
                                            303,
                                            304,
                                            305,
                                            306,
                                            307,
                                            308,
                                            309,
                                            310,
                                            311,
                                            312,
                                            313,
                                            314,
                                            315,
                                            316,
                                            317,
                                            318,
                                            319,
                                            320,
                                            321,
                                            322,
                                            323,
                                            324,
                                            325
                                        ],
                                        'asdtextsortenteilsigle': 'H',
                                        'asdmaterialteilsigle': null,
                                        'asdtextsortennummer': '1',
                                        'ewtextsortenteilsigle': null,
                                        'ewmaterialteilsigle': null,
                                        'ewtextsortennummer': null,
                                        'layerExtendValueArray': [
                                            'Cambridge University Library, Arthur Schnitzler Papers',
                                            null,
                                            'A140',
                                            'hsl. Werkniederschrift'
                                        ],
                                        'wTeilSigle': '',
                                        'uTeilSigle': 'H1'
                                    },
                                    {
                                        'nodename': 'T1',
                                        'tooltip': 'msl. Werkniederschrift',
                                        'value': 2,
                                        'docIdList': [
                                            326,
                                            327
                                        ],
                                        'asdtextsortenteilsigle': 'T',
                                        'asdmaterialteilsigle': null,
                                        'asdtextsortennummer': '1',
                                        'ewtextsortenteilsigle': null,
                                        'ewmaterialteilsigle': null,
                                        'ewtextsortennummer': null,
                                        'layerExtendValueArray': [
                                            'Cambridge University Library, Arthur Schnitzler Papers',
                                            null,
                                            'A140',
                                            'msl. Werkniederschrift'
                                        ],
                                        'wTeilSigle': '',
                                        'uTeilSigle': 'T1'
                                    }
                                ]
                            },
                            {
                                'nodename': 'A141',
                                'tooltip': 'A141',
                                'children': [
                                    {
                                        'nodename': '1',
                                        'tooltip': '1',
                                        'children': [
                                            {
                                                'nodename': 'S<sup>T</sup>1',
                                                'tooltip': 'msl. Skizze 1',
                                                'value': 2,
                                                'docIdList': [
                                                    328,
                                                    329
                                                ],
                                                'asdtextsortenteilsigle': 'S',
                                                'asdmaterialteilsigle': 'T',
                                                'asdtextsortennummer': '1',
                                                'ewtextsortenteilsigle': null,
                                                'ewmaterialteilsigle': null,
                                                'ewtextsortennummer': null,
                                                'layerExtendValueArray': [
                                                    'Cambridge University Library, Arthur Schnitzler Papers',
                                                    null,
                                                    'A141',
                                                    '1',
                                                    'msl. Skizze 1'
                                                ],
                                                'wTeilSigle': '',
                                                'uTeilSigle': 'S<sup>T</sup>1'
                                            }
                                        ]
                                    },
                                    {
                                        'nodename': '2',
                                        'tooltip': '2',
                                        'children': [
                                            {
                                                'nodename': 'S<sup>T</sup>2',
                                                'tooltip': 'msl. Skizze 2',
                                                'value': 6,
                                                'docIdList': [
                                                    330,
                                                    331,
                                                    332,
                                                    333,
                                                    334,
                                                    335
                                                ],
                                                'asdtextsortenteilsigle': 'S',
                                                'asdmaterialteilsigle': 'T',
                                                'asdtextsortennummer': '2',
                                                'ewtextsortenteilsigle': null,
                                                'ewmaterialteilsigle': null,
                                                'ewtextsortennummer': null,
                                                'layerExtendValueArray': [
                                                    'Cambridge University Library, Arthur Schnitzler Papers',
                                                    null,
                                                    'A141',
                                                    '2',
                                                    'msl. Skizze 2'
                                                ],
                                                'wTeilSigle': '',
                                                'uTeilSigle': 'S<sup>T</sup>2'
                                            }
                                        ]
                                    },
                                    {
                                        'nodename': '3',
                                        'tooltip': '3',
                                        'children': [
                                            {
                                                'nodename': 'T1',
                                                'tooltip': 'msl. Werkniederschrift',
                                                'value': 174,
                                                'docIdList': [
                                                    336,
                                                    337,
                                                    338,
                                                    339,
                                                    340,
                                                    341,
                                                    342,
                                                    343,
                                                    344,
                                                    345,
                                                    346,
                                                    347,
                                                    348,
                                                    349,
                                                    350,
                                                    351,
                                                    352,
                                                    353,
                                                    354,
                                                    355,
                                                    356,
                                                    357,
                                                    358,
                                                    359,
                                                    360,
                                                    361,
                                                    362,
                                                    363,
                                                    364,
                                                    365,
                                                    366,
                                                    367,
                                                    368,
                                                    369,
                                                    370,
                                                    371,
                                                    372,
                                                    373,
                                                    374,
                                                    375,
                                                    376,
                                                    377,
                                                    378,
                                                    379,
                                                    380,
                                                    381,
                                                    382,
                                                    383,
                                                    384,
                                                    385,
                                                    386,
                                                    387,
                                                    388,
                                                    389,
                                                    390,
                                                    391,
                                                    392,
                                                    393,
                                                    394,
                                                    395,
                                                    396,
                                                    397,
                                                    398,
                                                    399,
                                                    400,
                                                    401,
                                                    402,
                                                    403,
                                                    404,
                                                    405,
                                                    406,
                                                    407,
                                                    408,
                                                    409,
                                                    410,
                                                    411,
                                                    412,
                                                    413,
                                                    414,
                                                    415,
                                                    416,
                                                    417,
                                                    418,
                                                    419,
                                                    420,
                                                    421,
                                                    422,
                                                    423,
                                                    424,
                                                    425,
                                                    426,
                                                    427,
                                                    428,
                                                    429,
                                                    430,
                                                    431,
                                                    432,
                                                    433,
                                                    434,
                                                    435,
                                                    436,
                                                    437,
                                                    438,
                                                    439,
                                                    440,
                                                    441,
                                                    442,
                                                    443,
                                                    444,
                                                    445,
                                                    446,
                                                    447,
                                                    448,
                                                    449,
                                                    450,
                                                    451,
                                                    452,
                                                    453,
                                                    454,
                                                    455,
                                                    456,
                                                    457,
                                                    458,
                                                    459,
                                                    460,
                                                    461,
                                                    462,
                                                    463,
                                                    464,
                                                    465,
                                                    466,
                                                    467,
                                                    468,
                                                    469,
                                                    470,
                                                    471,
                                                    472,
                                                    473,
                                                    474,
                                                    475,
                                                    476,
                                                    477,
                                                    478,
                                                    479,
                                                    480,
                                                    481,
                                                    482,
                                                    483,
                                                    484,
                                                    485,
                                                    486,
                                                    487,
                                                    488,
                                                    489,
                                                    490,
                                                    491,
                                                    492,
                                                    493,
                                                    494,
                                                    495,
                                                    496,
                                                    497,
                                                    498,
                                                    499,
                                                    500,
                                                    501,
                                                    502,
                                                    503,
                                                    504,
                                                    505,
                                                    506,
                                                    507,
                                                    508,
                                                    509
                                                ],
                                                'asdtextsortenteilsigle': 'T',
                                                'asdmaterialteilsigle': null,
                                                'asdtextsortennummer': '1',
                                                'ewtextsortenteilsigle': null,
                                                'ewmaterialteilsigle': null,
                                                'ewtextsortennummer': null,
                                                'layerExtendValueArray': [
                                                    'Cambridge University Library, Arthur Schnitzler Papers',
                                                    null,
                                                    'A141',
                                                    '3',
                                                    'msl. Werkniederschrift'
                                                ],
                                                'wTeilSigle': '',
                                                'uTeilSigle': 'T1'
                                            }
                                        ]
                                    },
                                    {
                                        'nodename': '',
                                        'tooltip': 'Mappe zu A141',
                                        'value': 4,
                                        'docIdList': [
                                            510,
                                            511,
                                            512,
                                            513
                                        ],
                                        'asdtextsortenteilsigle': null,
                                        'asdmaterialteilsigle': null,
                                        'asdtextsortennummer': null,
                                        'ewtextsortenteilsigle': null,
                                        'ewmaterialteilsigle': null,
                                        'ewtextsortennummer': null,
                                        'layerExtendValueArray': [
                                            'Cambridge University Library, Arthur Schnitzler Papers',
                                            null,
                                            'A141',
                                            'Mappe zu A141'
                                        ],
                                        'wTeilSigle': '',
                                        'uTeilSigle': ''
                                    }
                                ]
                            },
                            {
                                'nodename': 'A193',
                                'tooltip': 'A193',
                                'children': [
                                    {
                                        'nodename': '2',
                                        'tooltip': '2',
                                        'children': [
                                            {
                                                'nodename': 'P<sup>H</sup>1',
                                                'tooltip': 'hsl. Stoffnotiz 1',
                                                'value': 2,
                                                'docIdList': [
                                                    5027,
                                                    5028
                                                ],
                                                'asdtextsortenteilsigle': null,
                                                'asdmaterialteilsigle': null,
                                                'asdtextsortennummer': null,
                                                'ewtextsortenteilsigle': 'P',
                                                'ewmaterialteilsigle': 'H',
                                                'ewtextsortennummer': '1',
                                                'layerExtendValueArray': [
                                                    'Cambridge University Library, Arthur Schnitzler Papers',
                                                    null,
                                                    'A193',
                                                    '2',
                                                    'hsl. Stoffnotiz 1'
                                                ],
                                                'wTeilSigle': 'P<sup>H</sup>1',
                                                'uTeilSigle': ''
                                            },
                                            {
                                                'nodename': 'P<sup>H</sup>2',
                                                'tooltip': 'hsl. Stoffnotiz 2',
                                                'value': 2,
                                                'docIdList': [
                                                    5037,
                                                    5038
                                                ],
                                                'asdtextsortenteilsigle': null,
                                                'asdmaterialteilsigle': null,
                                                'asdtextsortennummer': null,
                                                'ewtextsortenteilsigle': 'P',
                                                'ewmaterialteilsigle': 'H',
                                                'ewtextsortennummer': '2',
                                                'layerExtendValueArray': [
                                                    'Cambridge University Library, Arthur Schnitzler Papers',
                                                    null,
                                                    'A193',
                                                    '2',
                                                    'hsl. Stoffnotiz 2'
                                                ],
                                                'wTeilSigle': 'P<sup>H</sup>2',
                                                'uTeilSigle': ''
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                'nodename': 'A42b',
                                'tooltip': 'A42b',
                                'children': [
                                    {
                                        'nodename': '1',
                                        'tooltip': '1',
                                        'children': [
                                            {
                                                'nodename': 'P<sup>T</sup>6',
                                                'tooltip': 'msl. Stoffnotiz 6',
                                                'value': 2,
                                                'docIdList': [
                                                    8544,
                                                    8545
                                                ],
                                                'asdtextsortenteilsigle': null,
                                                'asdmaterialteilsigle': null,
                                                'asdtextsortennummer': null,
                                                'ewtextsortenteilsigle': 'P',
                                                'ewmaterialteilsigle': 'T',
                                                'ewtextsortennummer': '6',
                                                'layerExtendValueArray': [
                                                    'Cambridge University Library, Arthur Schnitzler Papers',
                                                    null,
                                                    'A42b',
                                                    '1',
                                                    'msl. Stoffnotiz 6'
                                                ],
                                                'wTeilSigle': 'P<sup>T</sup>6',
                                                'uTeilSigle': ''
                                            },
                                            {
                                                'nodename': 'P<sup>T</sup>8',
                                                'tooltip': 'msl. Stoffnotiz 8',
                                                'value': 2,
                                                'docIdList': [
                                                    8546,
                                                    8547
                                                ],
                                                'asdtextsortenteilsigle': null,
                                                'asdmaterialteilsigle': null,
                                                'asdtextsortennummer': null,
                                                'ewtextsortenteilsigle': 'P',
                                                'ewmaterialteilsigle': 'T',
                                                'ewtextsortennummer': '8',
                                                'layerExtendValueArray': [
                                                    'Cambridge University Library, Arthur Schnitzler Papers',
                                                    null,
                                                    'A42b',
                                                    '1',
                                                    'msl. Stoffnotiz 8'
                                                ],
                                                'wTeilSigle': 'P<sup>T</sup>8',
                                                'uTeilSigle': ''
                                            },
                                            {
                                                'nodename': 'P<sup>T</sup>7',
                                                'tooltip': 'msl. Stoffnotiz 7',
                                                'value': 2,
                                                'docIdList': [
                                                    8548,
                                                    8549
                                                ],
                                                'asdtextsortenteilsigle': null,
                                                'asdmaterialteilsigle': null,
                                                'asdtextsortennummer': null,
                                                'ewtextsortenteilsigle': 'P',
                                                'ewmaterialteilsigle': 'T',
                                                'ewtextsortennummer': '7',
                                                'layerExtendValueArray': [
                                                    'Cambridge University Library, Arthur Schnitzler Papers',
                                                    null,
                                                    'A42b',
                                                    '1',
                                                    'msl. Stoffnotiz 7'
                                                ],
                                                'wTeilSigle': 'P<sup>T</sup>7',
                                                'uTeilSigle': ''
                                            },
                                            {
                                                'nodename': 'P<sup>T</sup>9',
                                                'tooltip': 'msl. Stoffnotiz 9',
                                                'value': 2,
                                                'docIdList': [
                                                    8550,
                                                    8551
                                                ],
                                                'asdtextsortenteilsigle': null,
                                                'asdmaterialteilsigle': null,
                                                'asdtextsortennummer': null,
                                                'ewtextsortenteilsigle': 'P',
                                                'ewmaterialteilsigle': 'T',
                                                'ewtextsortennummer': '9',
                                                'layerExtendValueArray': [
                                                    'Cambridge University Library, Arthur Schnitzler Papers',
                                                    null,
                                                    'A42b',
                                                    '1',
                                                    'msl. Stoffnotiz 9'
                                                ],
                                                'wTeilSigle': 'P<sup>T</sup>9',
                                                'uTeilSigle': ''
                                            }
                                        ]
                                    },
                                    {
                                        'nodename': '2',
                                        'tooltip': '2',
                                        'children': [
                                            {
                                                'nodename': 'P<sup>T</sup>5',
                                                'tooltip': 'msl. Stoffnotiz 5',
                                                'value': 2,
                                                'docIdList': [
                                                    8552,
                                                    8553
                                                ],
                                                'asdtextsortenteilsigle': null,
                                                'asdmaterialteilsigle': null,
                                                'asdtextsortennummer': null,
                                                'ewtextsortenteilsigle': 'P',
                                                'ewmaterialteilsigle': 'T',
                                                'ewtextsortennummer': '5',
                                                'layerExtendValueArray': [
                                                    'Cambridge University Library, Arthur Schnitzler Papers',
                                                    null,
                                                    'A42b',
                                                    '2',
                                                    'msl. Stoffnotiz 5'
                                                ],
                                                'wTeilSigle': 'P<sup>T</sup>5',
                                                'uTeilSigle': ''
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                'nodename': 'A233',
                                'tooltip': 'A233',
                                'children': [
                                    {
                                        'nodename': '1',
                                        'tooltip': '1',
                                        'children': [
                                            {
                                                'nodename': 'P<sup>T</sup>10',
                                                'tooltip': 'msl. Stoffnotiz 10',
                                                'value': 2,
                                                'docIdList': [
                                                    8554,
                                                    8555
                                                ],
                                                'asdtextsortenteilsigle': null,
                                                'asdmaterialteilsigle': null,
                                                'asdtextsortennummer': null,
                                                'ewtextsortenteilsigle': 'P',
                                                'ewmaterialteilsigle': 'T',
                                                'ewtextsortennummer': '10',
                                                'layerExtendValueArray': [
                                                    'Cambridge University Library, Arthur Schnitzler Papers',
                                                    null,
                                                    'A233',
                                                    '1',
                                                    'msl. Stoffnotiz 10'
                                                ],
                                                'wTeilSigle': 'P<sup>T</sup>10',
                                                'uTeilSigle': ''
                                            },
                                            {
                                                'nodename': 'P<sup>T</sup>12',
                                                'tooltip': 'msl. Stoffnotiz 12',
                                                'value': 2,
                                                'docIdList': [
                                                    8556,
                                                    8557
                                                ],
                                                'asdtextsortenteilsigle': null,
                                                'asdmaterialteilsigle': null,
                                                'asdtextsortennummer': null,
                                                'ewtextsortenteilsigle': 'P',
                                                'ewmaterialteilsigle': 'T',
                                                'ewtextsortennummer': '12',
                                                'layerExtendValueArray': [
                                                    'Cambridge University Library, Arthur Schnitzler Papers',
                                                    null,
                                                    'A233',
                                                    '1',
                                                    'msl. Stoffnotiz 12'
                                                ],
                                                'wTeilSigle': 'P<sup>T</sup>12',
                                                'uTeilSigle': ''
                                            },
                                            {
                                                'nodename': 'P<sup>T</sup>11',
                                                'tooltip': 'msl. Stoffnotiz 11',
                                                'value': 2,
                                                'docIdList': [
                                                    9165,
                                                    9166
                                                ],
                                                'asdtextsortenteilsigle': null,
                                                'asdmaterialteilsigle': null,
                                                'asdtextsortennummer': null,
                                                'ewtextsortenteilsigle': 'P',
                                                'ewmaterialteilsigle': 'T',
                                                'ewtextsortennummer': '11',
                                                'layerExtendValueArray': [
                                                    'Cambridge University Library, Arthur Schnitzler Papers',
                                                    null,
                                                    'A233',
                                                    '1',
                                                    'msl. Stoffnotiz 11'
                                                ],
                                                'wTeilSigle': 'P<sup>T</sup>11',
                                                'uTeilSigle': ''
                                            },
                                            {
                                                'nodename': 'P<sup>T</sup>13',
                                                'tooltip': 'msl. Stoffnotiz 13',
                                                'value': 2,
                                                'docIdList': [
                                                    9167,
                                                    9168
                                                ],
                                                'asdtextsortenteilsigle': null,
                                                'asdmaterialteilsigle': null,
                                                'asdtextsortennummer': null,
                                                'ewtextsortenteilsigle': 'P',
                                                'ewmaterialteilsigle': 'T',
                                                'ewtextsortennummer': '13',
                                                'layerExtendValueArray': [
                                                    'Cambridge University Library, Arthur Schnitzler Papers',
                                                    null,
                                                    'A233',
                                                    '1',
                                                    'msl. Stoffnotiz 13'
                                                ],
                                                'wTeilSigle': 'P<sup>T</sup>13',
                                                'uTeilSigle': ''
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                'nodename': 'A257',
                                'tooltip': 'A257',
                                'children': [
                                    {
                                        'nodename': '11',
                                        'tooltip': '11',
                                        'children': [
                                            {
                                                'nodename': '',
                                                'tooltip': 'Umschlagblatt zu A257,11',
                                                'value': 2,
                                                'docIdList': [
                                                    8566,
                                                    8567
                                                ],
                                                'asdtextsortenteilsigle': null,
                                                'asdmaterialteilsigle': null,
                                                'asdtextsortennummer': null,
                                                'ewtextsortenteilsigle': null,
                                                'ewmaterialteilsigle': null,
                                                'ewtextsortennummer': null,
                                                'layerExtendValueArray': [
                                                    'Cambridge University Library, Arthur Schnitzler Papers',
                                                    null,
                                                    'A257',
                                                    '11',
                                                    'Umschlagblatt zu A257,11'
                                                ],
                                                'wTeilSigle': '',
                                                'uTeilSigle': ''
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                'nodename': 'Wienbibliothek im Rathaus',
                'tooltip': 'Wienbibliothek im Rathaus',
                'children': [
                    {
                        'nodename': 'N<sup>H</sup>1',
                        'tooltip': 'hsl. Figurenverzeichnis',
                        'value': 2,
                        'docIdList': [
                            8558,
                            8559
                        ],
                        'asdtextsortenteilsigle': 'N',
                        'asdmaterialteilsigle': 'H',
                        'asdtextsortennummer': '1',
                        'ewtextsortenteilsigle': null,
                        'ewmaterialteilsigle': null,
                        'ewtextsortennummer': null,
                        'layerExtendValueArray': [
                            'Wienbibliothek im Rathaus',
                            'hsl. Figurenverzeichnis'
                        ],
                        'wTeilSigle': '',
                        'uTeilSigle': 'N<sup>H</sup>1'
                    },
                    {
                        'nodename': '',
                        'tooltip': 'Umschlag zu H.I.N. 217.366',
                        'value': 4,
                        'docIdList': [
                            8560,
                            8561,
                            10921,
                            10922
                        ],
                        'asdtextsortenteilsigle': null,
                        'asdmaterialteilsigle': null,
                        'asdtextsortennummer': null,
                        'ewtextsortenteilsigle': null,
                        'ewmaterialteilsigle': null,
                        'ewtextsortennummer': null,
                        'layerExtendValueArray': [
                            'Wienbibliothek im Rathaus',
                            'Umschlag zu H.I.N. 217.366'
                        ],
                        'wTeilSigle': '',
                        'uTeilSigle': ''
                    }
                ]
            },
            {
                'nodename': 'Deutsches Literaturarchiv Marbach, A:Schnitzler',
                'tooltip': 'Deutsches Literaturarchiv Marbach, A:Schnitzler',
                'children': [
                    {
                        'nodename': 'Mp 218',
                        'tooltip': 'Mp 218',
                        'children': [
                            {
                                'nodename': 'P<sup>T</sup>4',
                                'tooltip': 'msl. Stoffnotiz 4',
                                'value': 2,
                                'docIdList': [
                                    9467,
                                    9468
                                ],
                                'asdtextsortenteilsigle': null,
                                'asdmaterialteilsigle': null,
                                'asdtextsortennummer': null,
                                'ewtextsortenteilsigle': 'P',
                                'ewmaterialteilsigle': 'T',
                                'ewtextsortennummer': '4',
                                'layerExtendValueArray': [
                                    'Deutsches Literaturarchiv Marbach, A:Schnitzler',
                                    'Mp 218',
                                    'msl. Stoffnotiz 4'
                                ],
                                'wTeilSigle': 'P<sup>T</sup>4',
                                'uTeilSigle': ''
                            },
                            {
                                'nodename': 'P<sup>T</sup>3',
                                'tooltip': 'msl. Stoffnotiz 3',
                                'value': 2,
                                'docIdList': [
                                    9587,
                                    9588
                                ],
                                'asdtextsortenteilsigle': null,
                                'asdmaterialteilsigle': null,
                                'asdtextsortennummer': null,
                                'ewtextsortenteilsigle': 'P',
                                'ewmaterialteilsigle': 'T',
                                'ewtextsortennummer': '3',
                                'layerExtendValueArray': [
                                    'Deutsches Literaturarchiv Marbach, A:Schnitzler',
                                    'Mp 218',
                                    'msl. Stoffnotiz 3'
                                ],
                                'wTeilSigle': 'P<sup>T</sup>3',
                                'uTeilSigle': ''
                            }
                        ]
                    }
                ]
            }
        ]
    };
    // delete 'undefined' nodes
    var clearUndifinedNodes = function (node, parent, pos) {
        if (node.children) {
            for (var i = 0; i < node.children.length; i++) {
                clearUndifinedNodes(node.children[i], node, i);
            }
        }
        if (node.nodename === 'undefined') {
            if (node.children) {
                if (parent) {
                    for (var i = node.children.length - 1; i >= 0; i--) {
                        parent.children.splice(pos, 0, node.children[i]);
                    }
                    parent.children.splice(pos + node.children.length, 1);
                }
            }
        }
    };
    clearUndifinedNodes(window.data5, null, 0);
    // (<any>window).ttreemap = new Ttreemap();
    t_treemap_1.Ttreemap.leafNumberAppendText = '&nbsp;.S';
    window.ttreemap = t_treemap_1.Ttreemap.init('div', 'tm', window.data5, 3, function (evt, docIds) {
        console.log(docIds);
    }, '');
    window.ttreemap.showLayerUsingNum(0);
});
// test for recreate treemap
// (<any>window).ttreemap ? (<any>window).ttreemap.showLayerUsingNum(0, undefined) : true;
// (<any>window).ttreemap.drawarea.remove();
// (<any>window).ttreemap ? (<any>window).ttreemap.showLayerUsingNum(1, undefined) : true;
// (<any>window).ttreemap = Ttreemap.init('div', 'tm', window.data3, true, (evt: any, docIds: any) => {
//   console.log(docIds);
// }, '');
// (<any>window).ttreemap ? (<any>window).ttreemap.showLayerUsingNum(1, undefined) : true;
//# sourceMappingURL=test.js.map