/**
* TCDH Tree-Map Library
*  --  for special representation of tree-data according to the requirements of TCDH Schnitzler-Project
*
* using svg.js
*
* @version 1.0.0
* @author Li Sheng <sheng@uni-trier.de>
*
*/
import { TtreemapType } from './t-treemap';
import * as SVG from 'svg.js';
/**
 *
 *
 *
 * @class TtreemapSVG
 */
export declare class TtreemapSVG implements TtreemapType {
    data: TtreemapNode;
    container: HTMLElement | null;
    containerId: string;
    drawarea: SVG.Doc;
    tooltip: SVG.Text;
    tooltipBG: SVG.Rect;
    leafImageUrl: string;
    maxLayerNum: number;
    calculateNodeValueBy: number;
    innerHightPercentTop: number;
    innerHightPercentBottom: number;
    innerWidthPercentBoth: number;
    innerWHMin: number;
    titlePointGap: number;
    TitleRowMin: number;
    TitleRowMax: number;
    cWidth: number;
    cHeight: number;
    leafPattern: SVG.Pattern;
    leafCallback: (evt: Event, docIds: any) => void;
    /**
     *
     * @param containerId
     * @param data
     * @param useLogorithms
     * @param leafCallback
     * @param leafImageUrl
     */
    init(containerId: string, data: any, calculateNodeValueBy: number, leafCallback: (evt: Event, docIds: any) => void, leafImageUrl: string): void;
    recreateTreeMap(): void;
    /**
     *
     *
     * @param {any} node
     * @returns number
     */
    calculateNodeValue(node: TtreemapNode | TtreemapLeaf, parentNode: TtreemapNode | null): number[];
    /**
     *
     *
     * @param {any} node
     * @returns number
     */
    calculateNodeValueOnlyOneLayer(node: TtreemapNode | TtreemapLeaf): number[];
    /**
     *
     *
     * @param {any} node
     * @param {any} parentInfo
     * @param {any} layerNum
     * @param {any} parentNode
     * @param {any} lsls -- lastSiblingLayers
     * @returns array
     */
    createTreeMap(node: TtreemapNode | TtreemapLeaf, parentInfo: any, layerNum: number, parentNode: TtreemapNode | TtreemapLeaf | null, lsls: CreateLayerReturnType | null): CreateLayerReturnType;
    /**
     *
     *
     * @param {any} node
     * @param {any} layerNum
     * @param {any} layerWidth
     * @param {any} layerHeight
     * @param {any} x
     * @param {any} y
     * @returns array
     */
    createLayer(node: TtreemapNode | TtreemapLeaf, layerNum: number, layerWidth: number, layerHeight: number, x: number, y: number): CreateLayerReturnType;
    /**
     *
     *
     * @param {any} node
     */
    expandLayer(node: TtreemapNode | TtreemapLeaf): void;
    hideLayer(node: TtreemapNode | TtreemapLeaf): void;
    /**
     * always show the first layer under this node
     *
     * @param {any} node
     */
    showLayer(node: TtreemapNode | TtreemapLeaf): void;
    getMaxLayerNum(): number;
    showLayerUsingNum(layerNum: number, node: TtreemapNode | TtreemapLeaf): void;
    hideAll(): void;
    showAll(node: TtreemapNode | TtreemapLeaf): void;
    setBlockCalcMethod(u: number): void;
    calculateLogarithms(n: number): 4 | 6 | 8 | 10;
    showTooltip(text: string, x: number, y: number): void;
    destroyTooltip(): void;
}
export interface CreateLayerReturnType {
    innerLayer: SVG.Rect;
    outerLayer: SVG.Rect;
    layerGroup: any;
    innerHeightTop: number;
}
export interface TtreemapNode {
    nodename: string;
    children: TtreemapNode[] | TtreemapLeaf[];
    expandButton?: any[];
    expanded?: boolean;
    treemapValues?: number[];
    layers?: CreateLayerReturnType;
    layerNum: number;
    tooltip: string;
}
export interface TtreemapLeaf {
    nodename: string;
    value: number;
    docIdList?: number[];
    treemapValues?: number[];
    layers?: CreateLayerReturnType;
    layerNum: number;
    tooltip: string;
}
/**
 * node data:
 * {
 *     layername: "",
 *     children: []
 *   }
 *
 * leaf data:
 * {
 *     layername: "",
 *     value: number
 * }
 */
