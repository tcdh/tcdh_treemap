/**
* TCDH Tree-Map Library
*  --  for special representation of tree-data according to the requirements of TCDH Schnitzler-Project
*
* based on jquery 3.2
*
* @version 1.0.0
* @author Li Sheng <sheng@uni-trier.de>
*
*/
define(["require", "exports", "jquery"], function (require, exports, $) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    /**
     *
     *
     * @export
     * @class TtreemapDiv
     * @implements {TtreemapType}
     */
    var TtreemapDiv = /** @class */ (function () {
        function TtreemapDiv() {
            this.data = {};
            this.container = {};
            this.containerId = '';
            this.drawarea = {};
            // tooltip: SVG.Text;
            // tooltipBG: SVG.Rect;
            this.leafImageUrl = '';
            this.maxLayerNum = -1;
            // setting parameters
            this.calculateNodeValueBy = 1; // 1: linear; 2: logarithms; 3: equal splitted
            this.nodeInnerLayerMargin = 4;
            this.innerWHMin = 0;
            // leafPattern: SVG.Pattern;
            this.TitleRowMax = 0;
            // text showed after number of leaves
            this.leafNumberAppendText = '';
            this.leafCallback = function () { };
        }
        /**
         *
         *
         * @param {string} containerId
         * @param {TtreemapNode} data
         * @param {number} calculateNodeValueBy
         * @param {(evt: Event, docIds: any) => void} leafCallback
         * @param {string} leafImageUrl
         * @memberof TtreemapDiv
         */
        TtreemapDiv.prototype.init = function (containerId, data, calculateNodeValueBy, leafCallback, leafImageUrl) {
            this.calculateNodeValueBy = calculateNodeValueBy;
            this.container = $('#' + containerId);
            this.containerId = containerId;
            this.data = data;
            this.leafImageUrl = leafImageUrl;
            this.leafCallback = leafCallback;
            this.cWidth = this.container.width();
            this.cHeight = this.container.height();
            this.maxLayerNum = -1;
            this.recreateTreeMap();
        };
        /**
         *
         *
         * @memberof TtreemapDiv
         */
        TtreemapDiv.prototype.recreateTreeMap = function () {
            // delete all layers
            if (this.drawarea && this.drawarea.remove) {
                this.drawarea.remove();
            }
            this.drawarea = $("<div class=\"ttm_div_drawarea\" id=\"" + (this.containerId + 'drawarea') + "\"></div>");
            this.container.append(this.drawarea);
            var _this = this;
            this.calculateNodeValue(this.data, null, 0);
            var layers = this.createTreeMap(this.data, this.drawarea, {
                width: this.cWidth,
                height: this.cHeight,
                parentNodeChildrenLength: 0
            }, 0, null, null);
            // check size
            this.checkAndAdjustSize(this.data);
        };
        /**
         *
         *
         * @param {(TtreemapNode | TtreemapLeaf)} node
         * @param {(TtreemapNode | null)} parentNode
         * @param {number} pos
         * @returns
         * @memberof TtreemapDiv
         */
        TtreemapDiv.prototype.calculateNodeValue = function (node, parentNode, pos) {
            var oriValue = 0;
            var normValue = 0;
            var equalSplitValue = 1;
            var normValueFromBottom = 0;
            node.tooltip = this.replaceBRtags(node.tooltip);
            if (parentNode !== null) {
                equalSplitValue = 1.0 / parentNode.children.length;
            }
            node.pos = pos;
            if (node.children) {
                var tempOriValue = 0;
                var tempNormValue = 0;
                var tempNormValueFromBottom = 0;
                for (var i = 0; i < node.children.length; i++) {
                    var vs = this.calculateNodeValue(node.children[i], node, i);
                    tempOriValue += vs[0];
                    tempNormValueFromBottom += vs[2];
                }
                oriValue += tempOriValue;
                if (oriValue < 2) { // too small value
                    normValue = this.calculateLogarithms(2);
                }
                else {
                    normValue = this.calculateLogarithms(oriValue);
                }
                // console.log(node.nodename + " " + oriValue + "  " + normValue);
                normValueFromBottom += tempNormValueFromBottom;
            }
            else {
                oriValue = node.value;
                if (node.value < 2) { // too small value
                    normValue = this.calculateLogarithms(2);
                }
                else {
                    normValue = this.calculateLogarithms(node.value);
                }
                normValueFromBottom = normValue;
            }
            node.treemapValues = [oriValue, normValue, normValueFromBottom, equalSplitValue];
            return node.treemapValues;
        };
        /**
         *
         *
         * @param {(TtreemapNode | TtreemapLeaf)} node
         * @returns
         * @memberof TtreemapDiv
         */
        TtreemapDiv.prototype.calculateNodeValueOnlyOneLayer = function (node) {
            var oriValue = node.treemapValues ? node.treemapValues[0] : 0;
            var normValue = node.treemapValues ? node.treemapValues[1] : 0;
            var normValueFromNextLayer = 0;
            if (node.children) {
                var tempNormValueFromNextLayer = 0;
                for (var i = 0; i < node.children.length; i++) {
                    var vs = node.children[i].treemapValues;
                    tempNormValueFromNextLayer += vs ? vs[1] : 0;
                }
                normValueFromNextLayer += tempNormValueFromNextLayer;
            }
            else {
                normValueFromNextLayer = node.treemapValues ? node.treemapValues[2] : 0;
            }
            return [oriValue, normValueFromNextLayer];
        };
        /**
         *
         *
         * @param {(TtreemapNode | TtreemapLeaf)} node
         * @memberof TtreemapDiv
         */
        TtreemapDiv.prototype.checkAndAdjustSize = function (node) {
            var nodeWidth = node.layers.innerLayer.width();
            var nodeHeight = node.layers.innerLayer.height();
            if (node.children) {
                if (node.layers.innerLayer.height() + node.layers.titleDiv.height() > node.layers.outerLayer.height()) {
                    var newHeight = node.layers.outerLayer.height() - node.layers.titleDiv.height();
                    node.layers.innerLayer.height(newHeight);
                    if (node.children) {
                        for (var i = 0; i < node.children.length; i++) {
                            var child = node.children[i];
                            var childNewHeight = newHeight;
                            if (node.layerNum % 2 === 1) {
                                var parentNodeChildrenLength = node.children.length;
                                childNewHeight = (newHeight - this.nodeInnerLayerMargin * (parentNodeChildrenLength - 1)) * child.treemapValues[3];
                            }
                            else {
                            }
                            child.layers.outerLayer.height(childNewHeight);
                        }
                    }
                }
            }
            if (node.children) {
                for (var i = 0; i < node.children.length; i++) {
                    var child = node.children[i];
                    this.checkAndAdjustSize(child);
                }
            }
        };
        /**
         *
         *
         * @param {(TtreemapNode | TtreemapLeaf)} node
         * @param {JQuery<HTMLElement>} parentDiv
         * @param {*} parentInfo
         * @param {number} layerNum
         * @param {(TtreemapNode | TtreemapLeaf | null)} parentNode
         * @param {(CreateLayerReturnType|null)} lsls
         * @returns {CreateLayerReturnType}
         * @memberof TtreemapDiv
         */
        TtreemapDiv.prototype.createTreeMap = function (node, parentDiv, parentInfo, layerNum, parentNode, lsls) {
            var _this_1 = this;
            var parentWidth = parentInfo.width;
            var parentHeight = parentInfo.height;
            var parentNodeChildrenLength = parentInfo.parentNodeChildrenLength;
            var isVertical = false;
            if (layerNum % 2 === 1) {
                isVertical = true;
            }
            else {
                isVertical = false;
            }
            var useValueNum = 0;
            if (this.calculateNodeValueBy === 2) {
                useValueNum = 1;
            }
            if (this.calculateNodeValueBy === 3) {
                useValueNum = 3;
            }
            var parentValue = parentNode !== null ? this.calculateNodeValueOnlyOneLayer(parentNode)[useValueNum] : this.calculateNodeValueOnlyOneLayer(node)[useValueNum];
            // calculate node width and height
            var nodeValue = node.treemapValues ? node.treemapValues[useValueNum] : 0;
            var nodeWidth = 0, nodeHeight = 0, marginTop = 0, marginLeft = 0;
            if (isVertical) {
                nodeWidth = 1.0 * nodeValue / parentValue * (parentWidth - this.nodeInnerLayerMargin * (parentNodeChildrenLength + 1));
                if (this.calculateNodeValueBy === 3) {
                    nodeWidth = (parentWidth - this.nodeInnerLayerMargin * (parentNodeChildrenLength + 1)) * nodeValue;
                    marginLeft = this.nodeInnerLayerMargin * (node.pos + 1) + nodeWidth * node.pos;
                }
                marginTop = 0;
                nodeHeight = parentHeight;
            }
            else {
                nodeWidth = parentWidth - this.nodeInnerLayerMargin * 2;
                nodeHeight = 1.0 * nodeValue / parentValue * (parentHeight - this.nodeInnerLayerMargin * (parentNodeChildrenLength + 1));
                if (this.calculateNodeValueBy === 3) {
                    nodeHeight = (parentHeight - this.nodeInnerLayerMargin * (parentNodeChildrenLength - 1)) * nodeValue;
                }
                if (node.pos === 0) {
                    marginTop = 0;
                }
                else {
                    marginTop = this.nodeInnerLayerMargin;
                }
            }
            if (!parentNode) {
                nodeWidth = parentWidth;
                nodeHeight = parentHeight;
            }
            // create top layer
            var inOutLayers = this.createLayer(node, parentNode, parentDiv, layerNum, nodeWidth, nodeHeight, marginTop, marginLeft);
            var innerLayer = inOutLayers.innerLayer;
            var outerLayer = inOutLayers.outerLayer;
            // check if child exists
            if (node.children) {
                var lastSiblingLayers = null;
                for (var i = 0; i < node.children.length; i++) {
                    var child = node.children[i];
                    lastSiblingLayers = this.createTreeMap(child, innerLayer, {
                        width: innerLayer.width(),
                        height: innerLayer.height(),
                        parentNodeChildrenLength: node.children.length
                    }, layerNum + 1, node, lastSiblingLayers);
                    innerLayer.append(lastSiblingLayers.outerLayer);
                }
            }
            else {
                var docIds_1 = node.docIdList;
                inOutLayers.innerLayer.click(function (evt) {
                    evt.stopPropagation();
                    _this_1.leafCallback(evt, docIds_1);
                });
            }
            return inOutLayers;
        };
        /**
         *
         *
         * @param {(TtreemapNode | TtreemapLeaf)} node
         * @param {(TtreemapNode | TtreemapLeaf | null)} parentNode
         * @param {JQuery<HTMLElement>} parentDiv
         * @param {number} layerNum
         * @param {number} layerWidth
         * @param {number} layerHeight
         * @param {number} marginTop
         * @param {number} marginLeft
         * @returns {CreateLayerReturnType}
         * @memberof TtreemapDiv
         */
        TtreemapDiv.prototype.createLayer = function (node, parentNode, parentDiv, layerNum, layerWidth, layerHeight, marginTop, marginLeft) {
            var _this_1 = this;
            var outerLayer = $('<div></div>')
                // .width(layerWidth).height(layerHeight)
                // .css('flex-grow', '' + (node.treemapValues ? node.treemapValues[2] : 1))
                .addClass('ttm_div_layer')
                .addClass('ttm_div_layer_' + layerNum)
                .css('width', layerWidth)
                .css('height', layerHeight)
                .css('margin-top', marginTop)
                // .css('margin-right', 0)
                .css('float', 'left')
                .attr('title', node.tooltip);
            if (parentNode) {
                outerLayer.css('margin-left', this.nodeInnerLayerMargin);
            }
            parentDiv.append(outerLayer);
            // title div
            var oriText = node.nodename;
            if (node.showValue) {
                oriText += '<span class="test"> (' + (node.treemapValues ? node.treemapValues[0] : 0) + '<span>' + this.leafNumberAppendText + '</span>' + ')</span>';
            }
            var oriTextForTitle = this.replaceBRtags(oriText);
            var titleDiv = $('<div></div>')
                // .width(layerWidth)
                .addClass('ttm_div_title').addClass('ttm_div_title_' + layerNum);
            var titleSpan = $('<span class="ttm_div_title_span"></span>').html(oriTextForTitle);
            titleDiv.append(titleSpan);
            titleDiv.attr('title', node.tooltip);
            if (node.children) {
                outerLayer.append(titleDiv);
            }
            else {
                titleDiv.height(0);
            }
            // try to make text shorter
            // let widthToShowText = outerLayer.bbox().width;
            // if ((<TtreemapNode>node).children) {
            //   widthToShowText = outerLayer.bbox().width - innerHeightTop - expandBtnPathGap * 2;
            // }
            // if (titleText.bbox().width > widthToShowText) {
            //   let percentOfToCutText = 1.0 * (titleText.bbox().width - widthToShowText) / titleText.bbox().width;
            //   let numToCut = Math.ceil(percentOfToCutText * node.nodename.length) + 4; // 3 points '...'
            //   let shortText = '...';
            //   if (numToCut < node.nodename.length) {
            //     shortText = node.nodename.substring(0, node.nodename.length - numToCut) + '...';
            //   }
            //   titleText.plain(shortText + ' (' + (node.treemapValues ? node.treemapValues[0] : 0) + ')');
            // }
            // inner layer
            var innerLayer = $('<div></div>')
                .addClass('ttm_div_layer_inner').addClass('ttm_div_layer_inner_' + layerNum);
            if (layerNum % 2 === 0) {
                innerLayer.addClass('ttm_div_layer_inner_horizontal');
            }
            else {
                innerLayer.addClass('ttm_div_layer_inner_vertical');
            }
            var innerLayerHeight = layerHeight - titleDiv.height();
            if (node.children) {
                innerLayerHeight -= this.nodeInnerLayerMargin;
            }
            innerLayer
                .css('width', layerWidth)
                .css('height', innerLayerHeight);
            outerLayer.append(innerLayer);
            innerLayerHeight = innerLayer.height();
            var innerLayerWidth = innerLayer.width();
            // add inner title text
            var innerTitleText = $("<span></span>").html(oriText)
                .addClass('innerTitleText');
            // check if should using vertical text
            if (node.children && innerLayerHeight / innerLayerWidth * 1.0 > 1.2) {
                innerTitleText.addClass('vertcalTitleText');
            }
            innerLayer.append(innerTitleText);
            // caculate title size
            var titleTextHeight = innerTitleText.height();
            var titleTextWidth = innerTitleText.width();
            var limit = 0.8; // max 80% of inner layer width or height
            var fontSize = 5, maxFontSize = 50;
            if (node.children) {
                if (innerLayerHeight / innerLayerWidth * 1.0 > 1) {
                    innerTitleText.addClass('vertcalTitleText');
                }
                while (titleTextHeight < innerLayerHeight * limit && titleTextWidth < innerLayerWidth * limit && fontSize < maxFontSize) {
                    fontSize++;
                    innerTitleText.css('font-size', fontSize + 'px');
                    titleTextHeight = innerTitleText.height();
                    titleTextWidth = innerTitleText.width();
                }
            }
            else {
                fontSize = 5, maxFontSize = 20;
                innerTitleText.addClass('innerTitleText_last');
                innerTitleText.css('font-size', fontSize + 'px');
                titleTextHeight = innerTitleText.height();
                titleTextWidth = innerTitleText.width();
                while (titleTextHeight < innerLayerHeight * limit && titleTextWidth < innerLayerWidth * limit && fontSize < maxFontSize) {
                    fontSize++;
                    if (fontSize < 12) {
                        limit = 0.9;
                    }
                    else {
                        limit = 0.8;
                    }
                    innerTitleText.css('font-size', fontSize + 'px');
                    titleTextHeight = innerTitleText.height();
                    titleTextWidth = innerTitleText.width();
                }
                // add background image in leaf
                var bgUrl = node.backgroundImageUrl;
                if (bgUrl) {
                    innerLayer.css('background-image', 'url(' + bgUrl + ')');
                }
            }
            titleTextHeight = innerTitleText.height();
            titleTextWidth = innerTitleText.width();
            marginLeft = (innerLayerWidth - titleTextWidth) / 2;
            marginTop = (innerLayerHeight - titleTextHeight) / 2;
            if (marginLeft < 0) {
                marginLeft = 0;
            }
            if (marginTop < 0) {
                marginTop = 0;
            }
            innerTitleText
                .css('margin-left', marginLeft)
                .css('margin-top', marginTop);
            // adjust offset of inner title
            var titleTextOffsetTop = innerTitleText.offset().top;
            var innerLayerOffsetTop = innerLayer.offset().top;
            if (titleTextOffsetTop !== innerLayerOffsetTop) {
                innerTitleText.offset({
                    top: innerLayerOffsetTop + marginTop,
                    left: innerTitleText.offset().left
                });
            }
            // add image to inner layer if the node is a leaf
            if (!node.children) {
                innerLayer.addClass('ttm_div_layer_inner_last');
                if (this.leafImageUrl !== '') {
                    // use user defined image
                    innerLayer.attr('background-image', 'url("' + this.leafImageUrl + '")');
                }
            }
            var expandBtn = null;
            // show-hide button
            if (node.children) {
                expandBtn = $('<div></div>')
                    .addClass('tmm_expand_btn_expand')
                    .addClass('tmm_expand_btn_expand_minus_' + layerNum);
                titleDiv.append(expandBtn);
                node.expanded = true;
                node.expandButton = expandBtn;
                // clicking on node opens its under layer
                outerLayer.click(function (evt) {
                    evt.stopPropagation();
                    _this_1.expandLayer(node);
                });
                innerLayer.click(function (evt) {
                    evt.stopPropagation();
                    _this_1.expandLayer(node);
                });
                titleDiv.click(function (evt) {
                    evt.stopPropagation();
                    _this_1.expandLayer(node);
                });
            }
            var ret = {
                innerLayer: innerLayer,
                titleDiv: titleDiv,
                titleSpan: titleSpan,
                outerLayer: outerLayer,
                innerTitleText: innerTitleText
            };
            node.layers = ret;
            if (this.maxLayerNum < layerNum) {
                this.maxLayerNum = layerNum;
            }
            node.layerNum = layerNum;
            return ret;
        };
        /**
         *
         *
         * @param {(TtreemapNode | TtreemapLeaf)} node
         * @memberof TtreemapDiv
         */
        TtreemapDiv.prototype.expandLayer = function (node) {
            if (node.expanded) {
                this.hideLayer(node);
            }
            else {
                this.showLayer(node);
            }
        };
        /**
         *
         *
         * @param {(TtreemapNode | TtreemapLeaf)} node
         * @memberof TtreemapDiv
         */
        TtreemapDiv.prototype.hideLayer = function (node) {
            // console.log(node.nodename + ': ' + node.expanded);
            if (node.children) {
                for (var i = 0; i < node.children.length; i++) {
                    var child = node.children[i];
                    child.layers ? child.layers.outerLayer.hide() : false;
                    if (child.children) {
                        var tcb = child.expandButton;
                    }
                    this.hideLayer(child);
                }
            }
            node.expanded = false;
            if (node.children) {
                var tcb = node.expandButton;
                tcb ? tcb.removeClass('tmm_expand_btn_expand_minus_' + node.layerNum) : true;
                tcb ? tcb.addClass('tmm_expand_btn_expand_plus_' + node.layerNum) : true;
                tcb ? tcb.show() : true;
            }
            // hide vertical text
            node.layers.innerTitleText.show();
            // hide title
            node.layers.titleSpan.hide();
            var nodeExpandButton = node.expandButton;
            nodeExpandButton ? nodeExpandButton.hide() : '';
        };
        /**
         * always show the first layer under this node
         *
         * @param {(TtreemapNode | TtreemapLeaf)} node
         * @memberof TtreemapDiv
         */
        TtreemapDiv.prototype.showLayer = function (node) {
            // console.log(node.nodename + ': ' + node.expanded);
            if (node.children) {
                for (var i = 0; i < node.children.length; i++) {
                    var child = node.children[i];
                    child.layers ? child.layers.outerLayer.show() : false;
                    if (child.children) {
                        var tcb = child.expandButton;
                        tcb ? tcb.removeClass('tmm_expand_btn_expand_minus_' + child.layerNum) : true;
                        tcb ? tcb.addClass('tmm_expand_btn_expand_plus_' + child.layerNum) : true;
                        tcb ? tcb.hide() : true;
                    }
                }
            }
            node.expanded = true;
            if (node.children) {
                var tcb = node.expandButton;
                tcb ? tcb.removeClass('tmm_expand_btn_expand_plus_' + node.layerNum) : true;
                tcb ? tcb.addClass('tmm_expand_btn_expand_minus_' + node.layerNum) : true;
                tcb ? tcb.show() : true;
                // hide vertical text
                node.layers.innerTitleText.hide();
                // show title
                node.layers.titleSpan.show();
                var nodeExpandButton = node.expandButton;
                nodeExpandButton ? nodeExpandButton.show() : '';
            }
        };
        /**
         *
         *
         * @returns
         * @memberof TtreemapDiv
         */
        TtreemapDiv.prototype.getMaxLayerNum = function () {
            return this.maxLayerNum;
        };
        /**
         *
         *
         * @param {number} layerNum
         * @param {(TtreemapNode | TtreemapLeaf)} node
         * @memberof TtreemapDiv
         */
        TtreemapDiv.prototype.showLayerUsingNum = function (layerNum, node) {
            if (typeof node === 'undefined') {
                this.hideAll();
            }
            node = typeof node !== 'undefined' ? node : this.data;
            if (node.layerNum <= layerNum) {
                this.showLayer(node);
            }
            if (node.children) {
                for (var i = 0; i < node.children.length; i++) {
                    var child = node.children[i];
                    this.showLayerUsingNum(layerNum, child);
                }
            }
        };
        /**
         *
         *
         * @memberof TtreemapDiv
         */
        TtreemapDiv.prototype.hideAll = function () {
            this.hideLayer(this.data);
        };
        /**
         *
         *
         * @param {(TtreemapNode | TtreemapLeaf)} node
         * @memberof TtreemapDiv
         */
        TtreemapDiv.prototype.showAll = function (node) {
            node = typeof node !== 'undefined' ? node : this.data;
            this.showLayer(node);
            if (node.children) {
                for (var i = 0; i < node.children.length; i++) {
                    var child = node.children[i];
                    this.showAll(child);
                }
            }
        };
        /**
         *
         *
         * @param {number} u
         * @memberof TtreemapDiv
         */
        TtreemapDiv.prototype.setBlockCalcMethod = function (u) {
            this.calculateNodeValueBy = u;
        };
        /**
         *
         *
         * @param {number} n
         * @returns
         * @memberof TtreemapDiv
         */
        TtreemapDiv.prototype.calculateLogarithms = function (n) {
            // return Math.log(n);
            // return Math.log(n) / Math.log(10);
            // return Math.log(n) / Math.LN10;
            if (n < 10) {
                return 4;
            }
            else if (n < 100) {
                return 6;
            }
            else if (n < 1000) {
                return 8;
            }
            else {
                return 10;
            }
        };
        /**
         *
         *
         * @private
         * @param {(JQuery<HTMLElement> | undefined)} outerLayer
         * @returns
         * @memberof TtreemapDiv
         */
        TtreemapDiv.prototype.getLayerWidth = function (outerLayer) {
            var res = (outerLayer !== undefined ? outerLayer.width() : 0);
            if (res !== undefined) {
                return res;
            }
            else {
                return 0;
            }
        };
        TtreemapDiv.prototype.replaceBRtags = function (str) {
            if (str) {
                str = str.replace(/<br>/g, '');
                str = str.replace(/<br\/>/g, '');
            }
            return str;
        };
        return TtreemapDiv;
    }());
    exports.TtreemapDiv = TtreemapDiv;
});
/**
 * node data:
 * {
 *     layername: "",
 *     children: []
 *   }
 *
 * leaf data:
 * {
 *     layername: "",
 *     value: number
 * }
 */
//# sourceMappingURL=t-treemap-div.js.map