module.exports = function(grunt) {

	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		uglify: {
			options: {
				banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
			},
			build: {
				src: 'dist/ttreemap.js',
				dest: 'dist/ttreemap.min.js'
			}
		},
		jsdoc: {
			dist: {
				src:[
					'dist/ttreemap.js',
					'README.md'
				],
				options: {
					destination: 'docs',
					template: 'utils/jsdoc_template',
					configure: 'jd_conf.json'
				}
			}
		},
		jshint: {
			options: {
				unused: true,
				undef: true,
				evil: true,
				shadow: true,
				globals: {
					'window': true,
					'console': true,
					'document': true,
					'define': true,
					'module': true,
					'svg': true,
					'jquery': true,
					'require': true,
					'setInterval': true,
					'clearInterval': true,
					'setTimeout': true,
				}
			},
			uses_defaults: ['dev/*.js']
		},
		qunit: {
			all: {
				options: {
					urls: [
						'test/index.html',
					]
				}
			}
		},
		shell: {
			options: {},
			command1: {
				command: 'ls'
			},
			command2: {
				command: 'ls'
			}
			
		}
	});

	// Load the plugins
	grunt.loadNpmTasks('grunt-contrib-jshint');
	grunt.loadNpmTasks('grunt-contrib-qunit');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-jsdoc');
	grunt.loadNpmTasks('grunt-shell');

	grunt.registerTask('default', ['uglify', 'jsdoc']);
	grunt.registerTask('dev', ['jshint']);

};