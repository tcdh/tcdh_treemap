TCDH Tree Map JS Library
==========================

Li Sheng  2017.09.18


## Requirements (if using SVG):

* svg.js
* Browser must support SVG (1.1)

## For development: 

```
	npm i -S -D 
	npm run dev-w
	npm run server-w

```

## How To Use:

### Installation: 

```
	npm i -S https://bitbucket.org/tcdh/tcdh_treemap.git
```

### Usage:

In HTML:

```
<!-- create a div in html as the container of the ttreemap -->
<div id="idOfContainer"></div>
	
```

In CSS:

```
// import the css of ttreemap
@import "~ttreemap/css/ttreemap-div.css";
// or
@import "~ttreemap/css/ttreemap-svg.css";
```

In SCRIPT:

```
// import ttreemap library
import { Ttreemap } from 'ttreemap';

// get data
const data = {...};

// init the tree map; 
// the first parameter tells the library to use 'svg' or 'div' to construct the treemap;
// the forth parameter tells the library if logarithms should be used for calculating size of blocks (has no effect for 'div')
const ttreemap = ttreemap.init('div', 'idOfContainer', data, 1/2/3, leafCallback, 'customLeafImageUrl');

// recreate the treemap:
ttreemap.drawarea.remove();
const ttreemap = ttreemap.init('div', 'idOfContainer', data, 1/2/3, leafCallback, 'customLeafImageUrl');

```

the data has typical tree structure. Values are only in leaves.
```
// leaf:
{
	nodename: string,
	tooltip: string,
	value: number
}
// node:
{
	nodename: string,
	tooltip: string,
	children: [] //array of nodes or leaves
}

// example:
/**
*       -- n2: 2
*      |
* n1 --| 
*      |
*      |         -- n5: 1
*       -- n3 --|
*                -- n4: 2
*/

{
	nodename: 'n1',
	tooltip: 'n1',
	children: [
		{
			nodename: 'n2',
			tooltip: 'n2',
			value: 2
		},
		{
			nodename: 'n3',
			tooltip: 'n3',
			children: [
				{
					nodename: 'n4',
					tooltip: 'n4',
					value: 1
				},
				{
					nodename: 'n5',
					tooltip: 'n5',
					value: 2
				}
			]
		}
	]
}
```

to hide all layers:
```
	ttreemap.hideAll();
```

to show all layers:
```
	ttreemap.showAll();
```

setup if using logorithms or other methods for block calculating:
```
	ttreemap.setBlockCalcMethod(1); // linear
	ttreemap.setBlockCalcMethod(2); // logarithms
	ttreemap.setBlockCalcMethod(3); // equal splitted
```

recreate the tree map:
```
	ttreemap.recreateTreeMap();
```

get maximal layer number:
```
	let layerNum = ttreemap.getMaxLayerNum();

```

show layer according to layer number:
```
	ttreemap.showLayerUsingNum(num);

```

### **some parameters for the size of title and edge areas:**

```
	// the height of tite area is 2% of the whole block
	ttreemap.innerHightPercentTop = 0.02;
	
	// the height of footer area is 1% of the whole block
	ttreemap.innerHightPercentBottom = 0.01;

	// the width of inner block is 98% of the whole block
	ttreemap.innerWidthPercentBoth = 0.01;
	
	// the minimal width of inner block is 50px
	ttreemap.innerWHMin = 50;

	// the expand button is 3px away from the top and bottom of the titel area
	ttreemap.titlePointGap = 3;

	// the minimal height of the title area is 15px
	ttreemap.TitleRowMin = 15;

	// after changing the parameters recreate the tree map:
	ttreemap.recreateTreeMap();

```

### **some additional properties in node data (if needed, then should be processed before calling init method):**

```
	// the tooltip text of this node
	node.tooltip: string
	
	// if show values after node titles
	node.showValue: boolean

```

### **change styles:**

The styles are defined in ttreemap.css.


